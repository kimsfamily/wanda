﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;



namespace BuzzEngine {

    [AddComponentMenu("UI/Effects/Gradiant")]

    public class Gradiant : BaseMeshEffect
    {
        public enum Type
        {
            Vertical,
            Horizontal
        }
        [SerializeField]
        public Type GradientType = Type.Vertical;

        [SerializeField]
        [Range(-1.5f, 1.5f)]
        public float Offset = 0f;

        [SerializeField]
        private Color32 StartColor = new Color(30,120,250,218);
        [SerializeField]
        private Color32 EndColor = new Color(0,30,60,218);

        public override void ModifyMesh(VertexHelper helper)
        {
            if (!IsActive() || helper.currentVertCount == 0)
                return;

            List<UIVertex> _vertexList = new List<UIVertex>();
            helper.GetUIVertexStream(_vertexList);

            int nCount = _vertexList.Count;
            switch (GradientType)
            {
                case Type.Vertical:
                    {
                        float fBottomY = _vertexList[0].position.y;
                        float fTopY = _vertexList[0].position.y;
                        float fYPos = 0f;

                        for (int i = nCount - 1; i >= 1; --i)
                        {
                            fYPos = _vertexList[i].position.y;
                            if (fYPos > fTopY)
                                fTopY = fYPos;
                            else if (fYPos < fBottomY)
                                fBottomY = fYPos;
                        }

                        float fUIElementHeight = 1f / (fTopY - fBottomY);
                        UIVertex v = new UIVertex();

                        for (int i = 0; i < helper.currentVertCount; i++)
                        {
                            helper.PopulateUIVertex(ref v, i);
                            v.color = Color32.Lerp(EndColor, StartColor, (v.position.y - fBottomY) * fUIElementHeight - Offset);
                            helper.SetUIVertex(v, i);
                        }
                    }
                    break;
                case Type.Horizontal:
                    {
                        float fLeftX = _vertexList[0].position.x;
                        float fRightX = _vertexList[0].position.x;
                        float fXPos = 0f;

                        for (int i = nCount - 1; i >= 1; --i)
                        {
                            fXPos = _vertexList[i].position.x;
                            if (fXPos > fRightX)
                                fRightX = fXPos;
                            else if (fXPos < fLeftX)
                                fLeftX = fXPos;
                        }

                        float fUIElementWidth = 1f / (fRightX - fLeftX);
                        UIVertex v = new UIVertex();

                        for (int i = 0; i < helper.currentVertCount; i++)
                        {
                            helper.PopulateUIVertex(ref v, i);
                            v.color = Color32.Lerp(EndColor, StartColor, (v.position.x - fLeftX) * fUIElementWidth - Offset);
                            helper.SetUIVertex(v, i);
                        }

                    }
                    break;
                default:
                    break;
            }
        }
    }

}
//#if !UNITY_5_1
//    public class UITextFX : BaseMeshEffect
//#else
//    public class UITextFX : BaseVertexEffect
//#endif
//{
//    [SerializeField]
//    private Color32 topColor = Color.white;
//    [SerializeField]
//    private Color32 bottomColor = Color.black;

//#if !UNITY_5_1
//    public override void ModifyMesh(Mesh mesh)
//    {
//        if (!this.IsActive())
//            return;

//        List<UIVertex> list = new List<UIVertex>();
//        using (VertexHelper vertexHelper = new VertexHelper(mesh))
//        {
//            vertexHelper.GetUIVertexStream(list);
//        }

//        ModifyVertices(list);  // calls the old ModifyVertices which was used on pre 5.2

//        using (VertexHelper vertexHelper2 = new VertexHelper())
//        {
//            vertexHelper2.AddUIVertexTriangleStream(list);
//            vertexHelper2.FillMesh(mesh);
//        }
//    }
//    public void ModifyVertices(List<UIVertex> vertexList)    
//#else
//    public override void ModifyVertices(List<UIVertex> vertexList)    
//#endif
//    {
//        int count = vertexList.Count;

//        if (count <= 0) return;

//        float bottomY = vertexList[0].position.y;
//        float topY = vertexList[0].position.y;

//        for (int i = 1; i < count; i++)
//        {
//            float y = vertexList[i].position.y;
//            if (y > topY)
//            {
//                topY = y;
//            }
//            else if (y < bottomY)
//            {
//                bottomY = y;
//            }
//        }

//        float uiElementHeight = topY - bottomY;

//        for (int i = 0; i < count; i++)
//        {
//            UIVertex uiVertex = vertexList[i];
//            uiVertex.color = Color32.Lerp(bottomColor, topColor, (uiVertex.position.y - bottomY) / uiElementHeight);
//            vertexList[i] = uiVertex;
//        }
//    }
//#if !UNITY_5_1
//    public override void ModifyMesh(VertexHelper vh)
//    {
//        //throw new System.NotImplementedException();
//    }
//#endif
//}