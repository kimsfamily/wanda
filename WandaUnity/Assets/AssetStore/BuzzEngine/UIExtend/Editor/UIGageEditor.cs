﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using BuzzEditor;

[CustomEditor(typeof(UIGage))]
public class UIGageEditor : BzEditor
{
    UIGage gage = null;

    protected override void OnEnable()
    {
        base.OnEnable();
        gage = (UIGage)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        gage.Value = EditorGUILayout.Slider(string.Format("Value({0:f2})",gage.fillValue),gage.Value,gage.min, gage.max);


        if (GUI.changed)
        {
            EditorUtility.SetDirty(gage);
            gage.bar.SetAllDirty();
        }
    }
}
