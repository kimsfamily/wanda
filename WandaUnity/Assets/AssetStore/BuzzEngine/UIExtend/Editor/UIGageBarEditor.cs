﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using BuzzEditor;
using UnityEditor.SceneManagement;

namespace BuzzEngine.UI
{
    [CustomEditor(typeof(UIGageBar))]
    public class UIGageBarEditor : BzEditor
    {
        public UIGageBar gage = null;
        Canvas canvas = null;

        //SerializedProperty fullTime;

        protected override void OnEnable()
        {
            base.OnEnable();
            gage = (UIGageBar)target;

            FindCanvas();

            //fullTime = serializedObject.FindProperty("fullTime");

        }
        void FindCanvas()
        {
            canvas = gage.gameObject.GetComponent<Canvas>();
            Transform target = gage.transform;

            while (canvas == null)
            {
                // 검사 후 캔버스가 없다면 하나 만들어 준다.   
                if ( target.parent == null)
                {
                    Canvas[] canvasList = GameObject.FindObjectsOfType<Canvas>();

                    canvas = gage.gameObject.AddComponent<Canvas>();
                    CanvasScaler scaler = gage.gameObject.AddComponent<CanvasScaler>();

                    foreach (Canvas c in canvasList)
                    {
                        canvas.renderMode = c.renderMode;
                        canvas.worldCamera = c.worldCamera;
                        canvas.planeDistance = c.planeDistance;
                        canvas.sortingLayerID = c.sortingLayerID;
                        canvas.sortingOrder = c.sortingOrder;

                        CanvasScaler cs = c.GetComponent<CanvasScaler>();
                        if(cs != null)
                        {
                            scaler.uiScaleMode = cs.uiScaleMode;
                            scaler.referenceResolution = cs.referenceResolution;
                            scaler.screenMatchMode = cs.screenMatchMode;
                            scaler.referencePixelsPerUnit = cs.referencePixelsPerUnit;
                        }

                        gage.gameObject.layer = c.gameObject.layer;    
                    }


                    break;
                }
                else
                {
                    canvas = target.parent.GetComponent<Canvas>();     
                }

                target = target.parent;
            }
        }
        public void AllObjectCheck()
        {

            Transform panel = gage.transform.FindChild("Panel");
            
            if (panel == null && gage.uiBar == null)
            {
                panel = CreateUIObject("Panel",gage.transform, false).transform;

                gage.name = "UIGageBar";
            }
            
            if (gage.uiBar == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Bar");
                if (obj != null) gage.uiBar = obj.gameObject.GetComponent<Image>();

                if (gage.uiBar == null)
                {
                    GameObject go = CreateUIObject("Bar", panel);
                    gage.uiBar = go.AddComponent<Image>();

                    gage.uiBar.type = Image.Type.Filled;
                    gage.uiBar.fillMethod = Image.FillMethod.Horizontal;
                    gage.uiBar.fillOrigin = 0;
                }
            }

            if (gage.uiFrame == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Frame");
                if (obj != null) gage.uiFrame = obj.gameObject.GetComponent<Image>();

                if (gage.uiFrame == null)
                {
                    GameObject go = CreateUIObject("Frame", panel);
                    gage.uiFrame = go.AddComponent<Image>();
                }
            }
            
            if (gage.uiText == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Text");
                if (obj != null) gage.uiText = obj.gameObject.GetComponent<Text>();

                if (gage.uiText == null)
                {
                    GameObject go = CreateUIObject("Text", panel);
                    gage.uiText = go.AddComponent<Text>();
                    gage.uiText.text = "Input Text";
                }
            }
        }
        

        GameObject CreateUIObject(string name, Transform parent, bool autoAnchor = true)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(parent, false);
            go.name = name;
            go.layer = canvas.gameObject.layer;

            RectTransform rTransform = go.AddComponent<RectTransform>();

            if (autoAnchor)
            {
                rTransform.anchorMin = Vector2.zero;
                rTransform.anchorMax = Vector2.one;
                rTransform.anchoredPosition = Vector2.zero;
                rTransform.sizeDelta = Vector2.zero;
            }
            
            return go;
        }
        public override void OnInspectorGUI()
        {
            AllObjectCheck();

            //serializedObject.Update();
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("fullTime"), true);
            //EditorGUILayout.PropertyField(fullTime);

            gage.fullTime = EditorGUILayout.FloatField("Full Time", gage.fullTime);

            gage.timerType = (UIGageBar.TimerType) EditorGUILayout.EnumPopup("Timer Type", gage.timerType);

            if( gage.uiBar != null)
                gage.fillValue = EditorGUILayout.Slider("fillValue(0~1)", gage.fillValue, 0f, 1f);

            EditorGUILayout.LabelField("Value To Time", gage.ValueToTime().ToString());

            gage.uiFrame.sprite = (Sprite)EditorGUILayout.ObjectField("Frame", gage.uiFrame.sprite, typeof(Sprite), true);
            gage.uiFrame.color = EditorGUILayout.ColorField("Frame Color", gage.uiFrame.color);

            gage.uiBar.sprite = (Sprite)EditorGUILayout.ObjectField("Bar", gage.uiBar.sprite, typeof(Sprite), false);
            gage.uiBar.color = EditorGUILayout.ColorField("Bar Color", gage.uiBar.color);

            gage.isAutoText = EditorGUILayout.Toggle("Auto Text Draw", gage.isAutoText);
            if( gage.isAutoText)
                gage.textFormat = EditorGUILayout.TextField("Text format", gage.textFormat);
            
            
                        
            gage.uiText.color = EditorGUILayout.ColorField("Text Color", gage.uiText.color);

            gage.useBlink = EditorGUILayout.Toggle("Use Blink" ,gage.useBlink);
            if(gage.useBlink)
            {
                gage.blinkDrawType = (UIGageBar.BlinkDrawType)EditorGUILayout.EnumPopup("Blink Draw Type", gage.blinkDrawType);

                if( gage.blinkDrawType == UIGageBar.BlinkDrawType.Color)
                {
                    gage.blinkColor = EditorGUILayout.ColorField("Blink Color", gage.blinkColor);
                }
                else
                {
                    for (int i = 0; i < gage.barSprites.Count; i++)
                    {
                        gage.barSprites[i] = (Sprite)EditorGUILayout.ObjectField("" + i, gage.barSprites[i], typeof(Sprite), false);
                    }

                    if (GUILayout.Button("Add Sprite"))
                    {
                        gage.barSprites.Add(null);
                    }
                }

                gage.blinkRatio = EditorGUILayout.FloatField("Blink Ratio", gage.blinkRatio);
                gage.blinkTimeGap = EditorGUILayout.FloatField("Blink Time Gap", gage.blinkTimeGap);
            }


            //if (SceneView.lastActiveSceneView == null || SceneView.lastActiveSceneView.camera == null || sm.target == null) return;

            //Vector3 temp = SceneView.lastActiveSceneView.camera.transform.position;
            //temp.x = sm.target.position.x;            
            //SceneView.lastActiveSceneView.pivot = temp;
            //SceneView.lastActiveSceneView.Repaint();

            if ( gage.isRun)
            {
                if (GUILayout.Button("Timer Stop"))
                {
                    gage.StopTimer();
                }
            }
            else
            {
                if (GUILayout.Button("Timer Test"))
                {
                    if( Application.isPlaying)
                    {
                        gage.StartTimer(gage.fullTime, null);
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Only You Can Test On Application Play", "Only You Can Test On Application Play", "Play");
                    }
                    
                }
            }
            

            //DrawDefaultInspector();
            
            if (GUI.changed || Application.isPlaying)
            {
                EditorUtility.SetDirty(gage);
                
                gage.Refresh();

                //serializedObject.ApplyModifiedProperties();

                if ( Application.isPlaying == false)
                // Unity Bug
                    EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
            }
        }
    }
}