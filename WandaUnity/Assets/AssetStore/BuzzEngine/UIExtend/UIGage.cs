﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGage : MonoBehaviour
{
    public Image bar;
    public float min = 0;
    public float max = 1;
    
    public float fillValue
    { get
        {
            return bar.fillAmount;
        }
        set
        {
            bar.fillAmount = value;
        }
    }

    public float Value
    {
        get { return fillValue * (max - min); }
        set { fillValue = value / (max - min); }
    }

    public void InitValue(float _min, float _max)
    {
        min = _min;
        max = _max;
    }
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
