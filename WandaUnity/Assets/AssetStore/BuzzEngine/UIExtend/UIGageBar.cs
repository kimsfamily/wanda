﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using System;

namespace BuzzEngine.UI
{
    public class UIGageBar : MonoBehaviour
    {
        public enum TimerType { Increase, Decrease}
        public enum BlinkDrawType { Color, Sprite, }
        [HideInInspector]
        public Image uiBar;
        [HideInInspector]
        public Image uiFrame;
        [HideInInspector]
        public Text uiText;
        
        public string text { get { return uiText.text; } set { uiText.text = value; } }

        public TimerType timerType = TimerType.Decrease;
        public BlinkDrawType blinkDrawType = BlinkDrawType.Color;
        
        public List<Sprite> barSprites = new List<Sprite>();

        public Color blinkColor = Color.white;

        public float fullTime = 60;
        public bool isRun = false;

        public float blinkRatio = 1;
        public float blinkTimeGap = 1;

        float blinkTimer = 0f;
        int blinkId = 0;

        public string textFormat = "Input Text {0:0}";

        public Action stopCallback = null;

        public bool isAutoText = false;

        public bool useBlink = false;

        Color barColor = Color.white;

        void Awake()
        {
            barColor = uiBar.color;
        }
        /// <summary>
        /// fillAmount of bar, 0 ~ 1
        /// </summary>
        public float fillValue
        {
            get
            {
                return uiBar.fillAmount;
            }
            set
            {
                uiBar.fillAmount = value;
            }
        }
        /// <summary>
        /// real value, min ~ max
        /// </summary>
        public float Value
        {
            get { return fillValue; }
            set
            {
                fillValue = value;
                if (isAutoText && uiText != null && uiText.gameObject.activeSelf)
                {
                    try {
                        uiText.text = string.Format(textFormat, ValueToTime());
                    }catch(FormatException e)
                    {
                        uiText.text = "textFormat code error : " + textFormat;
                        Debug.LogError(e.ToString());
                    }
                }
            }
        }
        
        // Update is called once per frame
        void Update()
        {
            if (isRun == false) return;

            switch (timerType)
            {
                case TimerType.Increase:
                    Increase();
                    break;
                default:
                    Decrease();
                    break;
            }
        }
        void Increase()
        {
            if (Value < 1f)
            {
                Value += Time.deltaTime / fullTime;

                // blinkRatio 수치 이하 부터 작동 한다.
                if (useBlink && Value >= blinkRatio) Blink();
            }
            else
            {
                Value = 1f;
            }
        }

        void Decrease()
        {
            if( Value > 0f)
            {
                Value -= Time.deltaTime / fullTime;

                // blinkRatio 수치 이하 부터 작동 한다.
                if (useBlink && Value <= blinkRatio) Blink();
            }
            else
            {
                Value = 0f;
            }
            
        }
        
        void Blink()
        {
            blinkTimer -= Time.deltaTime;

            if (blinkTimer <= 0)
            {
                if (blinkDrawType == BlinkDrawType.Color)
                {
                    uiBar.color = uiBar.color == blinkColor ? barColor : blinkColor;
                }
                else
                {
                    // 깜빡이 시스템을 사용 할지 스프라이트의 존재를 통해 확인한다.
                    if (barSprites == null || barSprites.Count == 0) return;

                    uiBar.sprite = barSprites[blinkId];
                    blinkId = blinkId + 1 >= barSprites.Count ? 0 : blinkId + 1;
                }
                
                if (blinkRatio <= 0 || blinkRatio >= 1)
                    blinkTimer = blinkTimeGap;
                else
                    blinkTimer = blinkTimeGap * (Value * 0.5f);
            }
        }
        /// <summary>
        /// fillValue zero
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return Value <= 0 ? true : false;
            }
        }
        /// <summary>
        /// fillValue > 1
        /// </summary>
        public bool IsFull
        {
            get
            {
                return Value >= 1 ? true : false;
            }
        }

        public float ValueToTime()
        {
            return fullTime * fillValue;
        }
        public void StartTimer(float _fullTime, Action callback, bool active = true)
        {
            gameObject.SetActive(active);

            isRun = true;
            fullTime = _fullTime;

            Value = timerType == TimerType.Increase ? 0 : 1f;

            stopCallback = callback;            
        }

        public void PauseTimer()
        {
            isRun = false;
        }
        public virtual void StopTimer(bool active = true)
        {
            if (isRun == false) return;

            isRun = false;
            if (stopCallback != null) stopCallback();

            if (blinkDrawType == BlinkDrawType.Color)
            {
                uiBar.color = barColor;
            }
            else
            {
                // 깜빡이 시스템을 사용 할지 스프라이트의 존재를 통해 확인한다.
                if (barSprites != null && barSprites.Count > 0)
                {
                    uiBar.sprite = barSprites[0];
                }

                
            }

#if UNITY_EDITOR
            Debug.Log(gameObject.name + " Timer Stop");
#endif

            gameObject.SetActive(active);

        }

        public void Visable(bool val)
        {
            uiBar.enabled = val;
            uiFrame.enabled = val;
            uiText.enabled = val;
        }

        public void Refresh()
        {
            Value = Value;
            uiBar.SetAllDirty();
        }
    }
}