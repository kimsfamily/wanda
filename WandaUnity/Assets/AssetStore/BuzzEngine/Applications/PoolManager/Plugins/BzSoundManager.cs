﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System.Xml;
using System.Xml.Serialization;
using System.Text;

using BuzzEngine;

namespace BuzzEngine
{
    public class BzSoundManager : BzMonoSingleton<BzSoundManager>
    {
        public enum FadeType
        {
            Normal, In, Out, Zero,
        }
        const string DatabasePath = "BuzzSoundManagerList";
        [System.Serializable]
        public class FileInfo<T>
        {
            public string name;
            public string bundleName;
            public string path;
            public List<T> childList = new List<T>();
            public bool foldOut = false;

            public bool isFolder = false;
        }

        [System.Serializable]
        public class ClipInfo
        {
            public string index;
            public string resourcePath;
            public string fullPath;
            public string bundleName;
            public string assetName;
            public AudioClip Clip { get; set; }
            public float volume = 1f;
        }
        public AudioSource bgSource;

        public List<AudioSource> oneSourceList {
            get; set;
        }

        public List<FileInfo<ClipInfo>> foldeList = new List<FileInfo<ClipInfo>>();

        public FadeType fadeType = FadeType.Normal;

        public float fadingTime = 0.5f;

        bool isLoaded = false;

        public bool fadeLock;

        public override void Init()
        {
            base.Init();
        }

        // Use this for initialization
        IEnumerator Start()// Awake()
        {
            bgSource = gameObject.AddComponent<AudioSource>();

            oneSourceList = new List<AudioSource>();

            oneSourceList.Clear();

            fadingTime = 0.5f;

            // 재생할 오디오 소스 컴포넌트를 하나 게임 오브젝트에 붙힌다.
            for (int i = 0; i < 1; i++)
            {
                CreateAudioSource();
            }

            LoadDataBase();

            yield return new WaitForSeconds(1f);

            if (foldeList == null) yield break;

            // 경로로 부터 clip파일을 초기화 한다.        
            foreach (FileInfo<ClipInfo> file in foldeList)
            {
                foreach (ClipInfo clip in file.childList)
                {
                    //Resource load
                    yield return BzResource.i.Load<AudioClip>(clip.resourcePath, a => clip.Clip = a);

                    //Bundle load
                    //yield return BzResource.i.Load<AudioClip>(clip.bundleName, clip.assetName, m => clip.Clip = m);
                }
            }

            isLoaded = true;
        }
        public Coroutine WaitLoadedFile()
        {
            return StartCoroutine(iWaitLoadedFile());
        }

        IEnumerator iWaitLoadedFile()
        {
            while (BzSoundManager.i.isLoaded == false)
            {
                yield return new WaitForFixedUpdate();
            }
        }
        public void LoadDataBase()
        {
            TextAsset textAsset = (TextAsset)Resources.Load(DatabasePath);
            XmlDocument xmldoc = new XmlDocument();

            if (textAsset == null) return;

            xmldoc.LoadXml(textAsset.text);

            foldeList = (List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>>)
                BuzzEngine.BzXmlSerializerHelper.Deserialize(xmldoc, typeof(List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>>));

            //string path = Application.dataPath + "/" + DatabasePath;        
            //var serializer = new XmlSerializer(typeof(List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>>));
            //var stream = new FileStream(path, FileMode.Open);
            //fileList = serializer.Deserialize(stream) as List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>>;
            //stream.Close();
        }

        public void SaveDataBase()
        {
#if !UNITY_WEBPLAYER
            string path = Application.dataPath + "/Resources/" + DatabasePath + ".xml";
            XmlDocument xmldoc = BuzzEngine.BzXmlSerializerHelper.Serialize(foldeList);
            StreamWriter outStream = System.IO.File.CreateText(path);
            xmldoc.Save(outStream);
            outStream.Close();
#endif
            //string path = Application.dataPath + "/" + DatabasePath;
            //var serializer = new XmlSerializer(typeof(List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>>));
            //var stream = new FileStream(path, FileMode.Create);
            //var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            //serializer.Serialize(xmlWriter, fileList);
            //stream.Close();
        }
        /// <summary>
        /// 재생을 위해 게임오브젝트에 오디오소스 컴포넌트를 붙힌다. (채널 개념이 된다.)
        /// </summary>
        void CreateAudioSource()
        {
            AudioSource ad = gameObject.AddComponent<AudioSource>();
            ad.playOnAwake = false;
            ad.loop = false;
            oneSourceList.Add(ad);
        }
        // 주로 효과음 처리시 너무 짧은 시간에 연속적으로 재생 요청이 들어 오는 경우에는 자동으로 Skip 처리를 해주자.
        float playSkipTimer = 0;
        float fadingVol = 0f;
        public float originVol = 1f;

        // Update is called once per frame
        void Update()
        {
            playSkipTimer -= Time.deltaTime;

            switch (fadeType)
            {
                case FadeType.In:
                    
                    fadingVol += (Time.timeScale != 0 ? Time.deltaTime : UnScaledTime.deltaTime) / fadingTime;

                    // set grobal volume
                    AudioListener.volume = originVol * fadingVol;

                    if (AudioListener.volume >= originVol)
                    {
                        AudioListener.volume = originVol;
                        fadeType = FadeType.Normal;
                    }

                    break;
                case FadeType.Out:

                    fadingVol -= (Time.timeScale != 0 ? Time.deltaTime : UnScaledTime.deltaTime) / fadingTime;

                    // set grobal volume
                    AudioListener.volume = originVol * fadingVol;

                    if (fadingVol <= 0)
                    {
                        AudioListener.volume = 0;
                        PauseAll();

                        fadeType = FadeType.Zero;
                    }
                    break;
            }
        }
        /// <summary>
        /// 미사용중인 AudioSource를 리턴한다.
        /// </summary>
        /// <returns></returns>
        AudioSource GetEmptyAudioSource()
        {
            int len = oneSourceList.Count;

            for (int i = 0; i < len; i++)
            {
                if (!oneSourceList[i].isPlaying)
                {
                    return oneSourceList[i];
                }
            }
            // if all using, return audio source after create Audio source
            CreateAudioSource();
            return oneSourceList[oneSourceList.Count - 1];
        }
        /// <summary>
        /// FX등의 단발설 음원 재생.
        /// </summary>
        /// <param name="name"></param>
        public void PlayOneShot(string name)
        {
            if (playSkipTimer > 0) return;
            playSkipTimer = 0.03f;

            AudioListener.volume = originVol;
            try
            {
                ClipInfo cInfo = GetClipInfo(name);

                AudioSource source = GetEmptyAudioSource();

                SetupClip(source, cInfo, false);

                source.PlayOneShot(cInfo.Clip);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Invalid Sound File Name : " + name + " " + e.ToString());
            }

        }
        /// <summary>
        /// BGM등의 긴 음원 재생.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isLoop"></param>
        public void PlayBG(string name, bool isLoop)
        {
            bgSource.Stop();

            ClipInfo cInfo = GetClipInfo(name);

            if (cInfo == null)
            {
                Debug.LogError("Audio file path : " + name);
                return;
            }

            SetupClip(bgSource, cInfo, isLoop);

            bgSource.Play();
        }
        void SetupClip(AudioSource source, ClipInfo info, bool loop)
        {
            source.clip = info.Clip;
            source.volume = info.volume;
            source.loop = loop;
        }
        /// <summary>
        /// Stop All AudioSource list
        /// </summary>
        public void StopAll()
        {
            int len = oneSourceList.Count;
            for (int i = 0; i < len; i++)
            {
                oneSourceList[i].Stop();
            }
        }
        void PauseAll()
        {
            if (oneSourceList == null) return;

            int len = oneSourceList.Count;
            for (int i = 0; i < len; i++)
            {
                oneSourceList[i].Pause();
            }
        }
        void ResumeAll()
        {
            if (oneSourceList == null) return;

            int len = oneSourceList.Count;
            for (int i = 0; i < len; i++)
            {
                oneSourceList[i].Play();
            }
        }
        /// <summary>
        /// Pause All AudioSource List
        /// </summary>
        public void Pause()
        {
            if (fadeType == FadeType.Normal)
            {
                fadingVol = 1;
                originVol = AudioListener.volume;
                fadeType = FadeType.Out;
            }
        }
        /// <summary>
        /// Resume All AudioSource Paused
        /// </summary>
        public void Resume()
        {
            if (fadeType != FadeType.Out)
            {
                fadingVol = 0;
                //ResumeAll();
                fadeType = FadeType.In;
            }
        }
        public void SetGlobalVolume(float _vol)
        {
            int len = oneSourceList.Count;
            for (int i = 0; i < len; i++)
            {
                oneSourceList[i].volume = _vol;
            }
        }
        public ClipInfo GetClipInfo(string name)
        {
            int fCount = foldeList.Count;
            for (int f = 0; f < fCount; f++)
            {
                int iCount = foldeList[f].childList.Count;
                for (int i = 0; i < iCount; i++)
                {
                    if (name.Equals(foldeList[f].childList[i].index))
                    {
                        return foldeList[f].childList[i];
                    }
                }
            }

            Debug.Log("Audio file path : " + name);
            return null;
        }
    }
}