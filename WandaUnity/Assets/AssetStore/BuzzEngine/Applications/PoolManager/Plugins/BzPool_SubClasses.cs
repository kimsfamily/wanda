﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BuzzEngine{
	[System.Serializable]
	public class PoolFolder{
		[HideInInspector]
		public string name;
		public Object folder;
        public bool foldout;
		public BzPPrefabInfo[] prefabInfos;
	}
	
	[System.Serializable]	
	public class BzPPrefabInfo{		
		[HideInInspector]
		public string name;
		public GameObject prefab;
		public int defaultCreateCount = 1 ;
		
		//Construct
		public BzPPrefabInfo(GameObject go){
			prefab = go;
			name = prefab.name + " (" + defaultCreateCount + ")";				
		}
	}
}
