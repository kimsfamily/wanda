using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BuzzEngine{

	//GPool ==> GPPrefabFolders ==> GPPrefabs ==> createObjects
	public class BzPoolManager : BzMonoSingleton<BzPoolManager>{

		//##################################################################################
		// ObjectCollection 
		//
		//
		//##################################################################################
		[System.Serializable]	
		/*
			수집용 이너 클래스 이다.
			생성된 동일한 오브텍트들을 담고 있다.
		*/
		public class GPCollection{
			// 풀 오브젝트 생성시 상위 부모 오브젝트.
			public Transform parent;
			// 풀 오브젝트의 프리팹.
			public List< GameObject > objectList = new List< GameObject >();

			// 프리팹 정보.
			public BzPPrefabInfo info;
			// 생성자.
			public GPCollection(BzPPrefabInfo _info){
				info = _info;
				// 상위 부모 오브젝트를 생성한다.
				GameObject go = new GameObject(info.name + "s" );
				// 부모 오브젝트를 최상위 관리 오브젝트 하위로 배치 한다.
				go.transform.parent = BzPoolManager.i.transform;
				parent = go.transform;
				// 기본적으로 만들어낼 오브젝트의 갯수를 별도 저장 하여 놓는다.
				// 추가 생성시 마다 별도 기재를 통해 전체 카운트를 파악 한다.
				info.defaultCreateCount = Mathf.Max( PlayerPrefs.GetInt(info.name,0), info.defaultCreateCount);
				// 오브젝트를 정해진 수만큼 만들어 낸다.
				for ( int i = 0; i < info.defaultCreateCount ; i++){
					RegistAfterCreateObject( false, Vector3.zero	 );
				}
			}

			// 사용할수 잇는 오브젝트를 만들어 낸다. 
			// 만들어진 것을 가져다 쓰는 것은 GetObject이다.
			public GameObject RegistAfterCreateObject (bool isActive,  Vector3 pos){
				GameObject go = Instantiate( info.prefab ) as GameObject; //cerate
				
				go.name = info.name;
				go.transform.parent = parent;
				go.transform.position = pos;													//postioning
				go.SetActive( isActive );										//active

                //GameObject gpgo = go.AddComponent< GameObject >();
                //gpgo.particle = gpgo.gameObject.GetComponent< ParticleSystem >();
				
				objectList.Add( go );

				return go;  //return
			}

			// 생성된 오브젝트를 가져온다.
			public GameObject GetObject( Vector3 pos ){
				//미 사용중인 객체를 찾아 활성화 시키고 위치를 조정후 return.
				for( int i = 0; i < objectList.Count ; i++){
					GameObject go = objectList[i].gameObject;
					if ( ! go.activeSelf ){ 	//search
						go.SetActive(true); 
						go.transform.position = pos;
						return objectList[ i ];
					}				
				}
				
				PlayerPrefs.SetInt(info.name,objectList.Count + 1);

				return RegistAfterCreateObject( true, pos );
			}

			public int UsedCount(bool used){
				int count = 0; 
				for( int i = 0; i < objectList.Count ; i++){
					if ( objectList[i].gameObject.activeSelf == used ){ 	//search
						count++;
					}				
				}
				return count;
			}
		}

		public PoolFolder[] folders;
		// 생성된 풀 오브젝트에 접근 할 수 있는 수집용 클래스를 딕셔너리 형태로 저장 한다.
		// 사용자는 키값을 가지고 사용 가능 한 오브젝트를 불러 올수 있다.
		//		public List<GPCollection> collections_debug = new List<GPCollection>();
		public Dictionary<string, GPCollection> collections = new Dictionary<string, GPCollection>();
	
		bool isInit = false;
		
		// use test
		// 실제 사용은 않하며 강제로 사용 가능한 오브젝틀 생성 할수 잇는 함수 이다.
		public void CreateObject(string key){
			PlayerPrefs.SetInt(collections[key].info.name,collections[key].objectList.Count + 1);
			collections[key].RegistAfterCreateObject(true, Vector3.zero);
		}
		
		// Use this for initialization.
		void Awake () {	
			// 플오브젝트를 생성 한다.
			CreateCollection();
			isInit = true;
		}


		public void RenamePrefabInfos(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){
					folders[fid].prefabInfos[i].name = folders[fid].prefabInfos[i].prefab.name + " (" + folders[fid].prefabInfos[i].defaultCreateCount + ")";
				}
			}
		}
		
		/// <summary>
		/// Renames this instance.
		/// 프리펩의 이름에 다가 현재 생성한 오브젝트의 갯수를 표기한다.
		/// </summary>
		public void LoadObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					folders[fid].prefabInfos[i].defaultCreateCount = PlayerPrefs.GetInt(folders[fid].prefabInfos[i].name,1);
				}
			}
		}
		
		public void SaveObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					PlayerPrefs.SetInt(folders[fid].prefabInfos[i].name, folders[fid].prefabInfos[i].defaultCreateCount);
				}
			}
		}
		// 풀 오브젝트를 생성 수집 한다.
		void CreateCollection(){
			
			//			collections_debug.Clear();
			// 클리어.
			collections.Clear();
			
			// 최상위 폴더 정보를 기준으로.
			for(int fid = 0; fid < folders.Length; fid++)
			{
				// 프리랩 정보를 기준으로 오브젝트를 생성한다.
				foreach ( BzPPrefabInfo info in folders[fid].prefabInfos )
				{
					// 컬렉션을 만든다.
					GPCollection collection = new GPCollection( info );	
					// 키로 컬렉션의 오브젝트를 불러 올수 있도록 등록 한다.
					// 이때 키값으로 이용 하기위에 에디터 상에서 info.name을 폴더.프리팹네임으로 등록 시켜놨다.
					//				collections_debug.Add( collection );
					collections.Add(  info.name, collection );
				}
			}
		}

		public int UsedCount(string key, bool used){
			return collections[key].UsedCount( used );
		}

		public int ObjectCount(string key){
			return collections[key].objectList.Count;
		}

		public bool CheckKey( string key ){
			if( ! collections.ContainsKey( key ) ){
                Debug.LogError("Invalid key : " + key);	
				return false;
			}else{
				return true;
			}
		}
        /// <summary>
        /// 재활용하지 않는 단발성 오브젝트를 생성한다.
        /// </summary>
        /// <returns></returns>
        public GameObject CreateObject(string key, Transform parentInfo)
        {
            GameObject go = Instantiate(collections[key].info.prefab) as GameObject;
            go.transform.position = parentInfo.position;
            go.transform.SetParent(parentInfo);
            return go;
        }
		/// <summary>
		/// Gets the object.
		/// 외부 클래스에서 본 함수를 통해 미리 생성된 객체를 가져다가 사용한다.
		/// 기본적으로 사용 하지 않는 객체(disable)를 가져 온다.
		/// 전부다 사용중일경우에는 내부적으로 생성 한다.
		/// </summary>
		/// <returns>
		/// The object.
		/// </returns>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='pos'>
		/// Position.
		/// </param>
		public GameObject UseObject( string key ,Vector3 pos) {			
			if( ! isInit ) return null;
			if( ! CheckKey ( key ) ) return null;

			return collections[ key ].GetObject( pos );
		}

		/// <summary>
		/// Hides the object.
		/// 소모한 object 로써 재사용을 위해 비활성화 시킨다.
		/// </summary>
		/// <param name='doomedObject'>
		/// Doomed object.
		/// </param>
		//		public void HideObject( GameObject doomedObject , float time) {	StartCoroutine( HideObjectTimer( doomedObject, time) );	}
		
		public void UnUseObject( GameObject doomedObject, float time = 0f ) 
		{
		    StartCoroutine(IEUnUseObject(doomedObject, time));	
		}
        IEnumerator IEUnUseObject(GameObject doomedObject, float time)
        {
            yield return new WaitForSeconds(time);

            if (collections.ContainsKey(doomedObject.name))
            {
                doomedObject.transform.parent = collections[doomedObject.name].parent;
                doomedObject.SetActive(false);
            }
        }
		// 모든 풀 오브젝트를 비활성화 시킨다.
		public void AllUnUseObject(){
			for(int fid = 0; fid < folders.Length; fid++){
				foreach ( BzPPrefabInfo info in folders[fid].prefabInfos ){
					GPCollection collection = collections[ info.name ];
					
					foreach( GameObject go in collection.objectList ){
						go.gameObject.SetActive(false);	
					}
				}				
			}
		}
		
	}//End Class
}
