﻿using UnityEditor;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.IO;

using BuzzEditor;
using System.Reflection;
using System;

namespace BuzzEngine.Sound
{
    [CustomEditor(typeof(BzSoundManager))]
    public class BzSoundManagerEditor : BzEditor
    {

        BzSoundManager sm = null;

        protected override void OnEnable()
        {
            base.OnEnable();
            sm = (BzSoundManager)target;

            // add audio source comp if is not exist Audio Source
            //if (sm.oneSourceList == null)
            //{
            //    sm.oneSourceList = new List<AudioSource>();
            //    AudioSource source = sm.gameObject.AddComponent<AudioSource>();
            //    sm.oneSourceList.Add(source);
            //}

            // XML데이터에서 세팅 정보를 로드 한다.
            sm.LoadDataBase();
            EditorUtility.SetDirty(sm);
        }

        public override void OnInspectorGUI()
        {
            float bWidth = 200f;

            //base.OnInspectorGUI();
            //{
            //    EditorGUILayout.Separator();
            //}

            sm.fadingTime = EditorGUILayout.FloatField("Fade Time", sm.fadingTime);

            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Load Dababase", GUILayout.Width(bWidth)))
                {
                    sm.LoadDataBase();
                    EditorUtility.SetDirty(sm);
                }
                if (GUILayout.Button("Save Dababase", GUILayout.Width(bWidth)))
                {
                    sm.SaveDataBase();
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Add Folder", GUILayout.Width(bWidth)))
                {
                    sm.foldeList.Add(new BzSoundManager.FileInfo<BzSoundManager.ClipInfo>());
                }
                if (GUILayout.Button("Refresh", GUILayout.Width(bWidth)))
                {
                    Refresh();
                    sm.SaveDataBase();
                    Repaint();
                    AssetDatabase.Refresh();
                }
            }
            EditorGUILayout.EndHorizontal();

            if (sm.foldeList == null) return;

            List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>> fileList = sm.foldeList;
            for (int f = 0; f < fileList.Count; f++)
            {
                BzSoundManager.FileInfo<BzSoundManager.ClipInfo> info = fileList[f];

                EditorGUILayout.BeginHorizontal();
                {
                    info.foldOut = EditorGUILayout.Foldout(info.foldOut, "");//, GUILayout.Width(30));

                    UnityEngine.Object pathObj = GetObject(info.path);

                    pathObj = (UnityEngine.Object)EditorGUILayout.ObjectField(pathObj, typeof(UnityEngine.Object), true, GUILayout.Width(200));

                    info.path = GetObjectPath(pathObj);

                    if (string.IsNullOrEmpty(info.path) == false && pathObj != null)
                    {
                        info.name = pathObj.name;
                    }
                    if (GUILayout.Button("Del", GUILayout.Width(100)))
                    {
                        fileList.RemoveAt(f);
                    }

                }
                EditorGUILayout.EndHorizontal();

                if (info.foldOut)
                {
                    // 들여쓰기.
                    EditorGUI.indentLevel++;

                    if (info.childList.Count == 0) EditorGUILayout.LabelField("Not Files");

                    foreach (var child in info.childList)
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            //if (GUILayout.Button("PLAY"))
                            //{
                            //    if (isPlaingClip == false) PlayClip(child.clip);
                            //    else StopClip(child.clip);
                            //}
                            //EditorGUILayout.ObjectField(child.clip, typeof(AudioClip));
                            BzEditorFunction.SetControlColor(Color.yellow);
                            EditorGUILayout.TextField(child.index);//.clip.name);
                            BzEditorFunction.SetControlColor();

                            // 들여쓰기. EditorGUI.indentLevel++;
                            child.volume = EditorGUILayout.Slider(child.volume, 0, 1);
                            //EditorGUI.indentLevel--;

                            //var t = (AudioClip)EditorGUILayout.ObjectField(child.name, child.clip, typeof(AudioClip));
                            //if (t != child.clip)
                            //{
                            //    child.clip = t;
                            //    child.name = t.name;
                            //}

                            if(isPlaying == false)
                            {
                                if (GUILayout.Button("▶", GUILayout.Width(30)))
                                {
                                    PlayClip(child);
                                }
                            }
                            else 
                            {
                                if (GUILayout.Button("■", GUILayout.Width(30)))
                                {
                                    //StopClip(child);
                                    StopAllClips();
                                }
                            }
                            
                        }
                        EditorGUILayout.EndHorizontal();
                    }

                    EditorGUI.indentLevel--;
                }
            }

            //update and redraw:
            if (GUI.changed)
            {
                EditorUtility.SetDirty(sm);
            }
        }
        /// <summary>
        /// 모든 폴더를 폴드 아웃으로 만들어 버린다.
        /// </summary>
        void FoldOut()
        {
            List<BzSoundManager.FileInfo<BzSoundManager.ClipInfo>> fileList = sm.foldeList;
            for (int f = 0; f < fileList.Count; f++)
            {
                fileList[f].foldOut = false;
            }
        }

        void Refresh()
        {
            if (sm.foldeList == null) return;

            foreach (BzSoundManager.FileInfo<BzSoundManager.ClipInfo> folder in sm.foldeList)
            {
                UnityEngine.Object pathObj = GetObject(folder.path);
                folder.path = GetObjectPath(pathObj);

                folder.childList.Clear();

                if (string.IsNullOrEmpty(folder.path) == false && pathObj != null)
                {
                    folder.name = pathObj.name;

                    AudioClip[] clips = GetAtFilePath<AudioClip>(folder.path);

                    foreach (AudioClip c in clips)
                    {
                        if (sm.GetClipInfo(c.name) == null)
                        {
                            BzSoundManager.ClipInfo cInfo = new BzSoundManager.ClipInfo();

                            cInfo.fullPath = GetObjectPath(c);

                            cInfo.bundleName = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(c)).assetBundleName = "bzsound";

                            cInfo.assetName = c.name;// AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(c)).name;

                            // 확장자를 제거 한다.
                            string temp = cInfo.fullPath.Substring(0, cInfo.fullPath.LastIndexOf(c.name) + c.name.Length);

                            // 패스를 기준으로 키를 만든다.
                            cInfo.index = folder.name + "/" + c.name;// cInfo.fullPath.Replace(folder.path, folder.name);

                            // 리소스폴더에 존재 한다는 가정 하에 불필요한 경로를 제거 한다.
                            cInfo.resourcePath = temp.Replace("Assets/Resources/", "");

                            folder.childList.Add(cInfo);
                        }
                    }
                }
            }
        }

        bool isPlaying = false;

        public void PlayClip(BzSoundManager.ClipInfo clipInfo)
        {
            AudioClip clip = AssetDatabase.LoadAssetAtPath<AudioClip>(clipInfo.fullPath);

            if (clip == null) Debug.LogError("Not Found AudioClip " + clipInfo.fullPath);

            else {
                PlayClip(clip,0,true);
                isPlaying = true;
            }
        }

        //public void PlayClip(AudioClip clip)
        //{
        //    Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
        //    System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        //    MethodInfo method = audioUtilClass.GetMethod(
        //        "PlayClip",
        //        BindingFlags.Static | BindingFlags.Public,
        //        null,
        //        new System.Type[] {
        //    typeof(AudioClip)
        //        },
        //        null
        //    );
        //    method.Invoke(
        //        null,
        //        new object[] {
        //    clip
        //        }
        //    );            
        //}
        public void StopClip(BzSoundManager.ClipInfo clipInfo)
        {
            AudioClip clip = AssetDatabase.LoadAssetAtPath<AudioClip>(clipInfo.fullPath);

            if (clip == null) Debug.LogError("Not Found AudioClip " + clipInfo.fullPath);
            else {
                StopClip(clip);
                isPlaying = false;
            }
        }
        public void StopClip(AudioClip clip)
        {
            Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
            System.Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
            MethodInfo method = audioUtilClass.GetMethod(
                "StopClip",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[] {
            typeof(AudioClip)
                },
                null
            );
            method.Invoke(
                null,
                new object[] {
            clip
                }
            );
        }

        public void PlayClip(AudioClip clip, float startTime, bool loop)
        {
            int startSample = (int)(startTime * clip.frequency);

            Assembly assembly = typeof(AudioImporter).Assembly;
            Type audioUtilType = assembly.GetType("UnityEditor.AudioUtil");

            Type[] typeParams = { typeof(AudioClip), typeof(int), typeof(bool) };
            object[] objParams = { clip, startSample, loop };

            MethodInfo method = audioUtilType.GetMethod("PlayClip", typeParams);
            method.Invoke(null, BindingFlags.Static | BindingFlags.Public, null, objParams, null);
        }

        //public void StopClip(AudioClip clip)
        //{
        //    Assembly assembly = typeof(AudioImporter).Assembly;
        //    Type audioUtilType = assembly.GetType("UnityEditor.AudioUtil");

        //    Type[] typeParams = { typeof(AudioClip), typeof(int), typeof(bool) };
        //    object[] objParams = { clip,null};

        //    MethodInfo method = audioUtilType.GetMethod("StopClip", typeParams);
        //    method.Invoke(null, BindingFlags.Static | BindingFlags.Public, null, objParams, null);
        //}

        public void StopAllClips()
        {
            isPlaying = false;
            Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
            Type audioUtilClass =
                  unityEditorAssembly.GetType("UnityEditor.AudioUtil");
            MethodInfo method = audioUtilClass.GetMethod(
                "StopAllClips",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[] { },
                null
            );
            method.Invoke(
                null,
                new object[] { }
            );
        }
    }
}