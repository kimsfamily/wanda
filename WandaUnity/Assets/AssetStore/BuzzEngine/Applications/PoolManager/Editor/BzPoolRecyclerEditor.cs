﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using BuzzEditor;

[CustomEditor(typeof(BzPoolRecycler))]
public class BzPoolRecyclerEditor : BzEditor
{
    BzPoolRecycler recycler;
	protected override void OnEnable()
    {
        base.OnEnable();
        recycler = (BzPoolRecycler)target;

        if (recycler.stopType == BzPoolRecycler.StopType.None)
            FindStopType();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        {
            EditorGUILayout.Separator();
        }

        BzPoolRecycler.StopType temp = (BzPoolRecycler.StopType)EditorGUILayout.EnumPopup("Type", recycler.stopType);

        if (temp != recycler.stopType)
        {
            recycler.stopType = temp;

            switch (recycler.stopType)
            {
                case BzPoolRecycler.StopType.Multi:
                    recycler.myAnimation = recycler.GetComponent<Animation>();
                    recycler.particle = recycler.GetComponent<ParticleSystem>();
                    break;
                case BzPoolRecycler.StopType.Animation:
                    recycler.myAnimation = recycler.GetComponent<Animation>();
                    break;
                case BzPoolRecycler.StopType.Particle:
                    recycler.particle = recycler.GetComponent<ParticleSystem>();
                    break;
            }
        }

        switch (recycler.stopType)
        {
            case BzPoolRecycler.StopType.Multi:
                recycler.myAnimation = (Animation) EditorGUILayout.ObjectField("Animation", recycler.myAnimation, typeof(Animation),true);
                recycler.particle = (ParticleSystem)EditorGUILayout.ObjectField("Particle", recycler.particle, typeof(ParticleSystem), true);
                break;
            case BzPoolRecycler.StopType.Animation:
                recycler.myAnimation = (Animation)EditorGUILayout.ObjectField("Animation", recycler.myAnimation, typeof(Animation), true);
                break;
            case BzPoolRecycler.StopType.Particle:
                recycler.particle = (ParticleSystem)EditorGUILayout.ObjectField("Particle", recycler.particle, typeof(ParticleSystem), true);
                break;
        }
    }


    void FindStopType()
    {
        recycler.myAnimation = recycler.GetComponent<Animation>();
        recycler.particle = recycler.GetComponent<ParticleSystem>();

        if (recycler.myAnimation != null)
        {
            recycler.stopType = BzPoolRecycler.StopType.Animation;
            return;
        }

        if (recycler.particle != null)
        {
            recycler.stopType = BzPoolRecycler.StopType.Particle;
            return;
        }

        Debug.LogError("Not Found Stop Type");
    }
}
