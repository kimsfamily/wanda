﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using BuzzEngine;
using BuzzEditor;

namespace BuzzEngine{
	[CustomEditor (typeof(BzPoolManager))]
	
	public class BzPoolManagerEditor : BzEditor {
		BzPoolManager pool;
		PoolFolder[] folders;

		
		protected override void OnEnable(){
			//i like bold handle labels since Target'm getting old:
			style.fontStyle = FontStyle.Bold;
			style.normal.textColor = Color.white;
			pool = (BzPoolManager)target;	
			folders = pool.folders;
		}
		public Object testKeyObject;
		public string testKey;
		
		public override void OnInspectorGUI(){
            if (Application.isPlaying) return;
			
			if( GUILayout.Button("Reset") ){
				ReloadObject();
				pool.SaveObjectCount();
			}
			
			if( Application.isPlaying ){
				GUILayout.BeginHorizontal();
				{
					testKeyObject = EditorGUILayout.ObjectField("test Object ", testKeyObject,typeof(Object),true);
					if( testKeyObject ) 
					{
						testKey = testKeyObject.name;
						if( GUILayout.Button("Create") ){
							pool.CreateObject(testKey);
						}
					}
				}
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginHorizontal();
			{
				if( GUILayout.Button("Load Count") ){
					pool.LoadObjectCount();
				}
				if( GUILayout.Button("Save Count") ){
					pool.SaveObjectCount();
				}
			}
			GUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("NAME");
                EditorGUILayout.LabelField("Count");
            } EditorGUILayout.EndHorizontal();

            PoolFolder[] folders = pool.folders;
            if (folders != null)
            {
                int fCount = folders.Length;
                for (int f = 0; f < fCount; f++)
                {
                    BzPPrefabInfo[] prefabInfos = folders[f].prefabInfos;

                    int pcount = prefabInfos.Length;

                    for (int p = 0; p < pcount; p++)
                    {
                        BzPPrefabInfo info = prefabInfos[p];

                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.TextField(info.name);
                            if (Application.isPlaying)
                                EditorGUILayout.TextField(pool.collections[info.name].objectList.Count + " / " + info.defaultCreateCount);
                            else
                                EditorGUILayout.TextField(info.defaultCreateCount.ToString());
                        } EditorGUILayout.EndHorizontal();
                    }
                }
            }

            /*

            fCount = folders.Length;
            for (int f = 0; f < fCount; f++)
            {
                PoolFolder folder = folders[f];

                EditorGUILayout.BeginHorizontal();

                folder.foldout = EditorGUILayout.Foldout(folder.foldout, "Folder");

                folder.folder = (UnityEngine.Object)EditorGUILayout.ObjectField(folder.folder, typeof(UnityEngine.Object));

                if (GUILayout.Button("Load"))
                {
                }
                if (GUILayout.Button("Del"))
                {
                }
                EditorGUILayout.EndHorizontal();

                if (folder.foldout)
                {
                    EditorGUI.indentLevel++;

                    if (folder.prefabInfos.Length == 0) EditorGUILayout.LabelField("Not Files");

                    foreach (var info in folder.clipList)
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            //EditorGUILayout.ObjectField(info.clip, typeof(AudioClip));
                            EditorFunction.SetControlColor(Color.yellow);
                            EditorGUILayout.TextField(info.clip.name);
                            EditorFunction.SetControlColor();
                            //EditorGUI.indentLevel++;

                            info.volume = EditorGUILayout.Slider(info.volume, 0, 1);

                            //EditorGUI.indentLevel--;
                            //var t = (AudioClip)EditorGUILayout.ObjectField(info.name, info.clip, typeof(AudioClip));
                            //if (t != info.clip)
                            //{
                            //    info.clip = t;
                            //    info.name = t.name;
                            //}
                        } EditorGUILayout.EndHorizontal();
                    }

                    EditorGUI.indentLevel--;
                }
            }
            */
                DrawDefaultInspector();
		}
		
		
		
		public void ReloadObject(){
			
			for(int fid = 0; fid < folders.Length; fid++){
				
				string path = AssetDatabase.GetAssetPath( folders[fid].folder );
				
				GameObject[] getObjects = GetAtFilePath< GameObject >( path );
				
				folders[ fid ].prefabInfos = new BzPPrefabInfo[ getObjects.Length ];
				
				folders[fid].name = path.Replace("Assets/","") + " " + getObjects.Length;
				
				// 해당 경로에 존재 하는 오브젝트를 읽어 들인다.
				for( int oid = 0; oid < getObjects.Length ; oid++){
					
					folders[ fid ].prefabInfos[oid] = new BzPPrefabInfo( getObjects[ oid ] );

					folders[ fid ].prefabInfos[oid].name = folders[ fid ].folder.name +"/"+ getObjects[ oid ].name;		

                    // 쓸모 없는 컴포넌트를 제거 한다.
                    System.Type type = System.Type.GetType("CFX_AutoDestructShuriken");
                    if (type != null)
                    {
                        var cfxs = getObjects[oid].GetComponents(type);
                        foreach (var c in cfxs) DestroyImmediate(c, true);
                    }
                    // 재활용 컴포넌트를 붙혀 준다.(없을 경우에만)
                    if( getObjects[oid].GetComponents<BzPoolRecycler>() == null) getObjects[oid].AddComponent<BzPoolRecycler>();
				}
			}
            //CreateEnum();

		}

        //void CreateEnum(){
        //    if( ! Directory.Exists( pool.enumPath ) ){
        //        Directory.CreateDirectory(pool.enumPath);
        //    }

        //    StreamWriter stream = new StreamWriter( new FileStream( pool.enumPath + "/" + pool.cShapFileName + ".cs" , FileMode.Create ) );
        //    string str = "// Auto Maker\n";

        //    str += "namespace GPool{\n";
        //    str += "// folder name + _ + prefab name\n";
        //    str += "\tpublic enum " + pool.cShapFileName + "{" + "\n";

        //    int fCount = folders.Length;
        //    for(int fid = 0; fid < fCount; fid++){
        //        int oCount = folders[fid].prefabInfos.Length;
        //        for( int oid = 0; oid < oCount ; oid++){
        //            str += "\t\t" + folders[fid].prefabInfos[oid].name + ",\n";
        //        }
        //    }
        //    str += "\t}" + "\n";
        //    str += "}" + "\n";

        //    stream.Write(str);
        //    stream.Close();

        //    EditorApplication.RepaintProjectWindow();
        //    AssetDatabase.Refresh();
        //}
	}
}
