﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using BuzzEngine;

/*
namespace GPool{
	[CustomEditor (typeof(GPParticle))]
	
	public class GPParticleEditor : GEditor {
		GPool.GPParticle pool;
		GPool.PoolFolder[] folders;
		
		
		protected override void OnEnable(){
			//i like bold handle labels since Target'm getting old:
			style.fontStyle = FontStyle.Bold;
			style.normal.textColor = Color.white;
			pool = (GPParticle)target;	
			folders = pool.folders;
		}
		public Object testKeyObject;
		public string testKey;
		
		public override void OnInspectorGUI(){		
			
			if( GUILayout.Button("Reset") ){
				ReloadObject();
				pool.SaveObjectCount();
			}
			
			if( Application.isPlaying ){
				GUILayout.BeginHorizontal();
				{
					testKeyObject = EditorGUILayout.ObjectField("test Object ", testKeyObject,typeof(Object),true);
					if( testKeyObject ) 
					{
						testKey = testKeyObject.name;
						if( GUILayout.Button("Create") ){
							pool.CreateObject(testKey);
						}
					}
				}
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginHorizontal();
			{
				if( GUILayout.Button("Load Count") ){
					pool.LoadObjectCount();
				}
				if( GUILayout.Button("Save Count") ){
					pool.SaveObjectCount();
				}
			}
			GUILayout.EndHorizontal();
			
			DrawDefaultInspector();
		}
		
		
		
		public void ReloadObject(){
			
			for(int fid = 0; fid < folders.Length; fid++){
				
				string path = AssetDatabase.GetAssetPath( folders[fid].folder );
				
				GameObject[] getObjects = GetAtFilePath< GameObject >( path );
				
				folders[ fid ].prefabInfos = new `BzPPrefabInfo[ getObjects.Length ];
				
				folders[fid].name = path.Replace("Assets/","") + " " + getObjects.Length;
				
				// 해당 경로에 존재 하는 오브젝트를 읽어 들인다.
				for( int oid = 0; oid < getObjects.Length ; oid++){
					
					folders[ fid ].prefabInfos[oid] = new `BzPPrefabInfo( getObjects[ oid ] );
					
					folders[ fid ].prefabInfos[oid].name = folders[ fid ].folder.name +"_"+ getObjects[ oid ].name;		
				}
			}
			CreateEnum();
			
		}
		
		void CreateEnum(){
			if( ! Directory.Exists( pool.enumPath ) ){
				Directory.CreateDirectory(pool.enumPath);
			}
			
			StreamWriter stream = new StreamWriter( new FileStream( pool.enumPath + "/" + pool.cShapFileName + ".cs" , FileMode.Create ) );
			string str = "// Auto Maker\n";
			
			str += "namespace GPool{\n";
			str += "\tpublic enum " + pool.cShapFileName + "{" + "\n";
			
			int fCount = folders.Length;
			for(int fid = 0; fid < fCount; fid++){
				int oCount = folders[fid].prefabInfos.Length;
				for( int oid = 0; oid < oCount ; oid++){
					str += "\t\t" + folders[fid].prefabInfos[oid].name + ",\n";
				}
			}
			str += "\t}" + "\n";
			str += "}" + "\n";
			
			stream.Write(str);
			stream.Close();
			
			EditorApplication.RepaintProjectWindow();
			AssetDatabase.Refresh();
		}
	}
}
*/