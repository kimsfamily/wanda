﻿using UnityEngine;
using System.Collections;

namespace BuzzEngine
{
    public class BzAnimator : MonoBehaviour
    {
        public Animator animator;
        public float playTime;

        void Update()
        {
            playTime = animator.GetCurrentAnimatorStateInfo(0).length;
        }

    }
}