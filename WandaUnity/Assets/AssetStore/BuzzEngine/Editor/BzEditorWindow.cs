﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BzEditorWindow : EditorWindow
{
    public const string MenuItemRoot = "DGate/";
    public const string ContextItemRoot = "DGate/";

    public static Color contentColor = Color.white;
    public static Color backgroundColor = Color.white;
    public static BzEditorWindow window = null;

    public static Vector2 toolSize = new Vector2(200, 745);
    public static float toolHeightOffset_FileView = 0;

    public static void InitWindow()
    {
        contentColor = GUI.contentColor;
        backgroundColor = GUI.backgroundColor;

        //window = (BEditorWindow)EditorWindow.GetWindow(typeof(BEditorWindow));
    }
    public void DrawLabel(string name, float width, float height)
    {
        BzEditorFunction.SetControlColor();
        EditorGUILayout.LabelField(name, EditorStyles.toolbarButton, GUILayout.Width(width), GUILayout.Height(height));
    }
    public bool DrawToggle(bool value, float width, float height)
    {
        BzEditorFunction.SetControlColor(Color.white, value ? Color.cyan : Color.white);
        return GUILayout.Toggle(value, value ? "■" : "", EditorStyles.toolbarButton, GUILayout.Width(width), GUILayout.Height(height));
        //BzEditorFunction.SetControlColor();
    }

    public bool DrawButtonToggle(string name, float width, float height, bool isSelect)
    {
        BzEditorFunction.SetControlColor(Color.white, isSelect ? Color.cyan : Color.white);

        bool rtVal = GUILayout.Button(name, EditorStyles.toolbarButton, GUILayout.Width(width), GUILayout.Height(height));

        BzEditorFunction.SetControlColor();

        return rtVal;
    }

    public bool DrawButton(string name, float width, float height)
    {
        return GUILayout.Button(name, EditorStyles.toolbarButton, GUILayout.Width(width), GUILayout.Height(height));
    }
    public bool DrawButton(string name)
    {
        return GUILayout.Button(name, EditorStyles.toolbarButton);
    }

    public void DrawTitle(string str)
    {
        EditorGUILayout.LabelField(str, EditorStyles.foldout);
    }
}
