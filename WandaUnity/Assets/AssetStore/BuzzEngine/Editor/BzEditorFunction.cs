﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

public class BzEditorFunction
{
    public static Color contentColor = Color.white;
    public static Color backgroundColor = Color.white;

    // control들의 색 변경 지원 함수
    public static void SetControlColor(Color? content = null, Color? back = null)
    {
        if (content == null) { GUI.contentColor = contentColor; }
        else { GUI.contentColor = content.Value; }

        if (back == null) { GUI.backgroundColor = backgroundColor; }
        else { GUI.backgroundColor = back.Value; }        
    }

    public static void OpenScene(string sceneName)
    {
        if (EditorSceneManager.GetActiveScene().isDirty)// EditorApplication.isSceneDirty)
            SaveScene();
        
        EditorApplication.isPaused = false;
        EditorApplication.isPlaying = false;
        //EditorApplication.OpenScene(sceneName + ".unity");
        EditorSceneManager.OpenScene(sceneName + ".unity");
        
    }
    public static bool SaveScene()
    {
        if (EditorUtility.DisplayDialog("Save Scene", "Save Current Scene : " + EditorSceneManager.GetActiveScene().path, "SAVE", "NO"))
        {
            EditorSceneManager.SaveOpenScenes();
            //EditorApplication.SaveScene();
            return true;
        }
        return false;
    }
    public static bool CreateScene(string sceneName)
    {
        if (EditorUtility.DisplayDialog("Create Scene", "Create (" + sceneName + ") Scene\nAre you OK? ", "Create", "NO"))
        {
            EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
            //EditorApplication.NewScene();
            return true;
        }
        
        return false;
    }
    static public T[] GetAtFilePath<T>(string path)
    {

        ArrayList al = new ArrayList();

        string[] fileEntries = Directory.GetFiles(path);

        foreach (string fileName in fileEntries)
        {
            UnityEngine.Object t = AssetDatabase.LoadAssetAtPath(fileName, typeof(T));

            if (t != null) al.Add(t);
        }

        T[] result = new T[al.Count];

        for (int i = 0; i < al.Count; i++) result[i] = (T)al[i];

        return result;
    }

    public static string GetAssetPath(string path)
    {
        int start = path.LastIndexOf("/Assets");
        if (start == -1) return path;

        int len = path.Length - start;
        return path.Substring(start, len);
    }
}
