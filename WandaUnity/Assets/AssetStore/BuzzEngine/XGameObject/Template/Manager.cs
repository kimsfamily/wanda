﻿using System.Collections;

namespace BuzzEngine.XGameObject.Template
{
    public class Manager : BuzzEngine.XGameObject.Manager
    {
        #region State & Species

        /// <summary>
        /// 게임내 캐릭터의 동작을 정의 한다.
        /// </summary>
        public enum eState
        {
            None = -1,
            Idle = 0
        }

        public enum eSpecies
        {
            Normal,
            Count,
        }

        public override int GetSpeciesCount()
        {
            return (int) eSpecies.Count;
        }

        public override string GetSpeciesFromIndex(int index)
        {
            return ((eSpecies) index).ToString();
        }

        #endregion

        // TemplateXGameObject pObjPrefab;


        public override IEnumerator Initialize()
        {
            //if (pObjPrefab == null)
            //{
            //    yield return RMGResource.i.Load<TemplateXGameObject>(RMG.GameTitles.AAplication, RMGResource.ePath.PlayObjects, "kingkong", v => pObjPrefab = v);
            //    if (pObjPrefab == null) { Debug.LogError("Not found resource : " + "kingkong"); yield break; }
            //}
            yield break;
        }

        protected override IEnumerator iSpawnToXGameObject(BuzzEngine.XGameObject.XSpawn spawn)
        {
            //Transform spawntf = spawn.transform;
            //TemplateXGameObject box = Instantiate(pObjPrefab, spawntf.position, spawntf.rotation) as Tile;
            //box.transform.parent = transform;
            //box.spawn = spawn;

            yield return null;
        }
    }

}