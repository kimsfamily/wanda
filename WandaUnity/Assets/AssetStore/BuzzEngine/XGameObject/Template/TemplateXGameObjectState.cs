﻿using UnityEngine;

namespace BuzzEngine.XGameObject.Template
{
    public abstract class TemplateXGameObjectState : BuzzEngine.XGameObject.XGameObjectState
    {
        public TemplateXGameObject my = null;

        public void ConvertXGameObject(BuzzEngine.XGameObject.XGameObject xObj)
        {
            my = (TemplateXGameObject) xObj;
        }
    }

    public class TemplateXGameObjectStateIdle : TemplateXGameObjectState
    {
        public override void Enter(BuzzEngine.XGameObject.XGameObject xObj)
        {
            ConvertXGameObject(xObj);
        }

        public override void Update()
        {

        }

        public override void OnCollisionEnter2D(Collision2D collision)
        {

        }

        public override void OnTriggerExit2D(Collider2D collision)
        {
        }
    }
}