﻿using UnityEngine;
using System;
using System.Collections;

namespace BuzzEngine.XGameObject
{
    public interface XState<T>
    {
        bool IsEnterable(T e);
        IEnumerator Initilize(T e);
        void Enter();
        void Update();
        void Exit();
    }

    //public interface ICollision2D
    //{
    //    void OnCollisionEnter2D(Collision2D collision);
    //    void OnCollisionStay2D(Collision2D collision);
    //    void OnCollisionExit2D(Collision2D collision);
    //}

    //public interface ITrigger2D
    //{
    //    void OnTriggerEnter2D(Collider2D collision);
    //    void OnTriggerStay2D(Collider2D collision);
    //    void OnTriggerExit2D(Collider2D collision);
    //}

    //public interface IGizmo
    //{
    //    void OnDrawGizmos();
    //    void OnDrawSeletedGizmos();
    //}

    public abstract class XGameObjectState
    {
        public virtual IEnumerator Initialize()
        {
            yield return null;
        }

        public virtual bool IsEnterable(BuzzEngine.XGameObject.XGameObject caller)
        {
            return !caller.EqualCurrentState(this);
        }
        public virtual void Enter(BuzzEngine.XGameObject.XGameObject xGameObject) { }
        public virtual void Update() { }
        public virtual void Exit() { }

        public virtual void OnCollisionEnter2D(Collision2D collision) { }
        public virtual void OnCollisionStay2D(Collision2D collision) { }
        public virtual void OnCollisionExit2D(Collision2D collision) { }
        public virtual void OnTriggerEnter2D(Collider2D collision) { }
        public virtual void OnTriggerStay2D(Collider2D collision) { }
        public virtual void OnTriggerExit2D(Collider2D collision) { }

        public virtual void OnCollisionEnter(Collision collision) { }
        public virtual void OnCollisionStay(Collision collision) { }
        public virtual void OnCollisionExit(Collision collision) { }
        public virtual void OnTriggerEnter(Collider collision) { }
        public virtual void OnTriggerStay(Collider collision) { }
        public virtual void OnTriggerExit(Collider collision) { }

        public virtual void OnDrawGizmos() { }
        public virtual void OnDrawSeletedGizmos() { }
    }
}