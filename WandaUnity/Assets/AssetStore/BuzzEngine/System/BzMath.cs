﻿using UnityEngine;
using System.Collections;

namespace BuzzEngine
{
    public static class BzMath
    {
        /// <summary>
        /// 두 점을 이은 직선의 각도를 리턴한다.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static float GetAngleOfLine(Vector2 p1, Vector2 p2)
        {
            float rtVal = Mathf.Atan2((p2.y - p1.y), (p2.x - p1.x)) * 180f / Mathf.PI;
            rtVal = (rtVal < 0 ? (360f + rtVal) : rtVal);
            rtVal = (rtVal > 360f ? (rtVal - 360f) : rtVal);
            return rtVal;
        }
        /// <summary>
        /// For 2D, direct를 기준으로 angle만큼 회전한 방향값을 리턴한다.
        /// </summary>
        /// <param name="direct"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Vector3 GetDirectByArcAngle(Vector3 direct, float angle)
        {
            Vector2 pPoint = new Vector2(0, 0);
            float fAngleTemp = angle * Mathf.PI / 180;

            //Only 2D Process
            pPoint.x = (direct.x * Mathf.Cos(fAngleTemp)) - (direct.y * Mathf.Sin(fAngleTemp));
            pPoint.y = (direct.x * Mathf.Sin(fAngleTemp)) + (direct.y * Mathf.Cos(fAngleTemp));

            return new Vector3(pPoint.x, pPoint.y, 0);
        }
        /// <summary>
        /// angle로 회전하는 물체 C주위에 있는 A,B의 좌표값 변화 시킨다.
        /// </summary>
        /// <param name="posA"></param>
        /// <param name="posB"></param>
        /// <param name="pCenter"></param>
        /// <param name="angle"></param>
        public static void ChanagePosByDirectByArcAngle(ref Vector3 posA, ref Vector3 posB, Vector3 pCenter, float angle)
        {

            Vector3 dirCA1 = GetDirectByArcAngle((pCenter - posA).normalized, angle);
            Vector3 dirCB1 = GetDirectByArcAngle((pCenter - posB).normalized, angle);

            posA = pCenter + dirCA1 * Vector3.Distance(pCenter, posA);
            posB = pCenter + dirCB1 * Vector3.Distance(pCenter, posB);
        }
        
        public static Vector3 GetPointByArcAngle(Vector3 basePos, Vector3 direct, float angle)
        {
            return basePos + GetDirectByArcAngle(direct, angle);
        }
        /// <summary>
        /// 한 직선의 점을 리턴한다.
        /// </summary>
        /// <param name="pos1"></param>
        /// <param name="pos2"></param>
        /// <param name="percentage"></param>
        /// <returns></returns>
        public static Vector3 GetPointPosToPos(Vector3 pos1, Vector3 pos2, float percentage)
        {
            Vector3 direct = pos2 - pos1;
            float distance = Vector3.Distance(pos2, pos1) * 0.9f;
            return pos1 + (direct.normalized * distance);
        }

        /// This is based off an explanation and expanded math presented by Paul Bourke:
        /// It takes two lines as inputs and returns true if they intersect, false if they don't.
        /// If they do, ptIntersection returns the point where the two lines intersect.  
        public static bool DoLinesIntersect(Vector2 L11, Vector2 L12, Vector2 L21, Vector2 L22, ref Vector2 intersection)
        {
            // Denominator for ua and ub are the same, so store this calculation
            float d = (L22.y - L21.y) * (L12.x - L11.x) - (L22.x - L21.x) * (L12.y - L11.y);
            //n_a and n_b are calculated as seperate values for readability
            float n_a = (L22.x - L21.x) * (L11.y - L21.y) - (L22.y - L21.y) * (L11.x - L21.x);
            float n_b = (L12.x - L11.x) * (L11.y - L21.y) - (L12.y - L11.y) * (L11.x - L21.x);

            // Make sure there is not a division by zero - this also indicates that
            // the lines are parallel.  
            // If n_a and n_b were both equal to zero the lines would be on top of each 
            // other (coincidental).  This check is not done because it is not 
            // necessary for this implementation (the parallel check accounts for this).
            if (d == 0) return false;

            // Calculate the intermediate fractional point that the lines potentially intersect.
            float ua = n_a / d;
            float ub = n_b / d;

            // The fractional point will be between 0 and 1 inclusive if the lines
            // intersect.  If the fractional calculation is larger than 1 or smaller
            // than 0 the lines would need to be longer to intersect.
            if (ua >= 0d && ua <= 1d && ub >= 0d && ub <= 1d)
            {
                intersection.x = L11.x + (ua * (L12.x - L11.x));
                intersection.y = L11.y + (ua * (L12.y - L11.y));
                return true;
            }
            return false;
        }
        /// <summary>
        /// 2 벡터의 중간점을 리턴한다.
        /// </summary>
        /// <param name="posA"></param>
        /// <param name="posB"></param>
        /// <returns></returns>
        public static Vector3 GetCenterPosFrom2Vector(Vector3 posA, Vector3 posB)
        {
            return new Vector3((posB.x + posA.x) / 2f, (posB.y + posA.y) / 2f, (posB.z + posA.z) / 2f);
        }
        public static Vector3 InversePoint(Vector3 basePos, Vector3 targetPos)
        {
            return 2f * basePos - targetPos;
        }

        public static Vector3 Get3DPointFrom2DPoint(Camera camera, Vector2 pos, float depth)
        {
            Ray ray = camera.ScreenPointToRay(pos);
            return ray.GetPoint(depth);
        }
        public static bool Convert3DPointFrom2DPoint(Camera camera, ref Vector3 pos3D, Vector2 pos2D, GameObject target)
        {
            RaycastHit hit = new RaycastHit();
            Ray ray = camera.ScreenPointToRay(pos2D);
            if (Physics.Raycast(ray.origin, ray.direction, out hit, 100))
            {
                if (hit.collider.gameObject == target)
                {
                    pos3D = hit.point;
                    return true;
                }
            }
            return false;
        }
        

        /// <summary>
        /// Calulates Position for RectTransform.position from a transform.position. Does not Work with WorldSpace Canvas!
        /// </summary>
        /// <param name="_Canvas"> The Canvas parent of the RectTransform.</param>
        /// <param name="_Position">Position of in world space of the "Transform" you want the "RectTransform" to be.</param>
        /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
        /// <returns></returns>
        public static Vector3 CalculatePositionFromTransformToRectTransform(this Canvas _Canvas, Vector3 _Position, Camera _Cam)
        {
            Vector3 Return = Vector3.zero;
            if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                Return = _Cam.WorldToScreenPoint(_Position);
            }
            else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
            {
                Vector2 tempVector = Vector2.zero;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(_Canvas.transform as RectTransform, _Cam.WorldToScreenPoint(_Position), _Cam, out tempVector);
                Return = _Canvas.transform.TransformPoint(tempVector);
            }

            return Return;
        }

        /// <summary>
        /// Calulates Position for RectTransform.position Mouse Position. Does not Work with WorldSpace Canvas!
        /// </summary>
        /// <param name="_Canvas">The Canvas parent of the RectTransform.</param>
        /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
        /// <returns></returns>
        public static Vector3 CalculatePositionFromMouseToRectTransform(this Canvas _Canvas, Camera _Cam)
        {
            Vector3 Return = Vector3.zero;

            if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                Return = Input.mousePosition;
            }
            else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
            {
                Vector2 tempVector = Vector2.zero;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(_Canvas.transform as RectTransform, Input.mousePosition, _Cam, out tempVector);
                Return = _Canvas.transform.TransformPoint(tempVector);
            }

            return Return;
        }

        /// <summary>
        /// Calculates Position for "Transform".position from a "RectTransform".position. Does not Work with WorldSpace Canvas!
        /// </summary>
        /// <param name="_Canvas">The Canvas parent of the RectTransform.</param>
        /// <param name="_Position">Position of the "RectTransform" UI element you want the "Transform" object to be placed to.</param>
        /// <param name="_Cam">The Camera which is used. Note this is useful for split screen and both RenderModes of the Canvas.</param>
        /// <returns></returns>
        public static Vector3 CalculatePositionFromRectTransformToTransform(this Canvas _Canvas, Vector3 _Position, Camera _Cam)
        {
            Vector3 Return = Vector3.zero;
            if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                Return = _Cam.ScreenToWorldPoint(_Position);
            }
            else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
            {
                RectTransformUtility.ScreenPointToWorldPointInRectangle(_Canvas.transform as RectTransform, _Cam.WorldToScreenPoint(_Position), _Cam, out Return);
            }
            return Return;
        }


    }
}
