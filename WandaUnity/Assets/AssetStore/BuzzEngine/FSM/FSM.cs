//-----------------------------------------------------------------------
// <copyright file="FSM.cs" company="CompanyName">
//     Company copyright tag.
// FSM<T>, FSM, FSM{T}
// </copyright>
//-----------------------------------------------------------------------
//--------------------------------------------------------
/* Copyright (c) Buzzment. All rights reserved.
 * author       : Heyya
 * date         : 11/08/2013 ~ 11/08/2016
 */
namespace BuzzEngine
{
    using System;
    using UnityEngine;
    /// <summary>
    /// FSM
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FSM<T>
    {
        /// <summary>
        ///     대상의 스테이트를 초기화 한다.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="initialState"></param>
        public void Initialize(T stateTarget,FSMState<T> initialState)
        {
            SetState(stateTarget,initialState);
        }

        #region FSM

        public string StateName = "Null";

        /// <summary>
        ///     current State
        /// </summary>
        public FSMState<T> State { get; private set; }

        /// <summary>
        ///     직전 스테이트
        /// </summary>
        private FSMState<T> _previousState;

        public void FSMUpdate()
        {
            if (State != null) State.Execute();
        }
        
        public void SetState(T stateTarget, FSMState<T> newState)
        {
            _previousState = State;

            if (_previousState != null)
                _previousState.Exit();

            if (newState != null)
            {
                State = newState;
                newState.Target = stateTarget;

                var str = newState.ToString();
                var startIndex = str.LastIndexOf(".", StringComparison.Ordinal) + 1;
                str = str.Substring(startIndex);
                StateName = str;
                
                newState.Enter();
            }
        }

        public void RevertState(T stateTarget)
        {
            if (_previousState != null)
                SetState(stateTarget, _previousState);
        }

        public void NextState()
        {
            if (State != null)
                State.Next();
        }
        
        #endregion
    }

    public class FSMMonoSingleton<T> : MonoBehaviour where T : FSMMonoSingleton<T>
    {
        #region FSM Copy From FSM<T>

        public string StateName = "Null";

        /// <summary>
        ///     current State
        /// </summary>
        public FSMState<T> State { get; private set; }

        /// <summary>
        ///     직전 스테이트
        /// </summary>
        private FSMState<T> _previousState;

        public void FSMUpdate()
        {
            if (State != null) State.Execute();
        }

        public void SetState(T stateTarget, FSMState<T> newState)
        {
            _previousState = State;

            if (_previousState != null)
                _previousState.Exit();

            if (newState != null)
            {
                State = newState;
                newState.Target = stateTarget;

                var str = newState.ToString();
                var startIndex = str.LastIndexOf(".", StringComparison.Ordinal) + 1;
                str = str.Substring(startIndex);
                StateName = str;

                newState.Enter();
            }
        }

        public void RevertState(T stateTarget)
        {
            if (_previousState != null)
                SetState(stateTarget, _previousState);
        }

        public void NextState()
        {
            if (State != null)
                State.Next();
        }

        #endregion
        #region Singleton

        /// <summary>
        ///     Instance
        /// </summary>
        private static T _staticInstance;

        //instance
        public static T I
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if (_staticInstance != null) return _staticInstance;

                _staticInstance = FindObjectOfType(typeof(T)) as T;

                // Object not found, we create a temporary one
                if ((_staticInstance == null) && Application.isPlaying)
                {
                    //Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                    _staticInstance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();

                    var parent = GameObject.Find("SingleTons") ?? new GameObject("SingleTons");

                    _staticInstance.transform.parent = parent.transform;

                    // Problem during the creation, this should not happen
                    if (_staticInstance == null)
                        Debug.LogError("Problem during the creation of " + typeof(T));
                }
                if (_staticInstance != null) _staticInstance.Init();
                return _staticInstance;
            }
        }

        //// If no other monobehaviour request the instance in an awake function
        //// executing before this one, no need to search the object.
        //private void Awake()
        //{
        //    if( _staticInstance == null )
        //    {
        //        _staticInstance = this as T;
        //        _staticInstance.Init();
        //    }
        //}

        // This function is called when the instance is used the first time
        // Put all the initializations you need here, as you would do in Awake
        public virtual void Init()
        {
        }

        // Make sure the instance isn't referenced anymore when the user quit, just in case.
        public void OnApplicationQuit()
        {
            _staticInstance = null;
        }

        public virtual void OnPause()
        {
        }

        #endregion
    }

    /// <summary>
    ///     하나의 단일 생명체(오브젝트)를 지칭하는 클래스
    /// </summary>
    public abstract class FSMMono<T> : MonoBehaviour where T : FSMMono<T>
    {
        /// <summary>
        ///     XGameObject를 등록 관리 하는 매니져 클래스
        ///     <para>캐릭터가 하이라키에 생성시 자동 등록 될수 있도록 OnEnable()에 등록 시켜 준다.</para>
        /// </summary>
        //public MonoFSMManager controler = null;

        public string StateName; 

        public Squeezin.InGame.XGameObjects.TileState Mtes;

        public FSMMonoState<T> State { get; private set; }
        
        public void ChangeState(T stateTarget,Enum e)
        {
            ChangeState(stateTarget, e.ToString());
        }
        public void ChangeState(T stateTarget, string className)
        {
            ChangeState(stateTarget, Type.GetType(className));
        }
        public void ChangeState(T stateTarget, Type type)
        {
            if ((State != null) && (type == State.GetType())) return;

            if (State != null)
            {
                _preState = State;
                State.Exit();
            }

            State = gameObject.AddComponent(type) as FSMMonoState<T>;

            if (State != null)
            {
                State.Mono = stateTarget;
                StateName = State.ToString();
                var si = StateName.LastIndexOf(".", StringComparison.Ordinal) + 1;
                
                StateName = StateName.Substring(si).Replace(")","");
            }
        }
        public bool EqualState(Enum e)
        {
            return EqualState(e.ToString());
        }
        public bool EqualState(string type)
        {
            return EqualState(Type.GetType(type));
        }
        public bool EqualState(Type type)
        {
            return (State != null) && (type == State.GetType());
        }

        private FSMMonoState<T> _preState;
        public void RevertState(T stateTarget)
        {
            ChangeState(stateTarget, _preState.GetType());
        }
        //public void RemoveState()
        //{
        //    if (_state != null)
        //        DestroyObject(_state);
        //}
    }

    //public abstract class FSMCharacter : FSMMono<FSMCharacter>
    //{
    //    ///// <summary>
    //    ///// 현 오브젝트의 팀네임(주로 캐릭터에 활용시 사용함)
    //    ///// <para>ex) Enemy, Player</para>
    //    ///// </summary>
    //    public MonoFSMManager.eTeam TeamKey = MonoFSMManager.eTeam.Enemy;
    //}
}