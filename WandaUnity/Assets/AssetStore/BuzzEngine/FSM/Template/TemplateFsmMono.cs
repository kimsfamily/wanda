﻿using UnityEngine;
using System.Collections;
using BuzzEngine;

namespace BuzzEngine.Template
{
    public class TemplateFsmMono : FSMMono<TemplateFsmMono>
    {
        #region State & Species

        /// <summary>
        /// 게임내 캐릭터의 동작을 정의 한다.
        /// </summary>
        public enum eState
        {
            None = -1,
            Idle = 0
        }

        public enum eSpecies
        {
            Normal,
            Count,
        }

        public int GetSpeciesCount()
        {
            return (int)eSpecies.Count;
        }

        public string GetSpeciesFromIndex(int index)
        {
            return ((eSpecies)index).ToString();
        }

        #endregion

        /// <summary>
        /// 활성화시 초기 스테이트를 결정한다.
        /// </summary>
        public eState startState = eState.None;

        public eSpecies species = eSpecies.Count;

        //public override void Awake()
        //{
        //    AddStateDic(eState.Idle, new TemplateFsmMonoStateIdle());
        //    // 모든 스테이트를 등록 후 반드시 각 스테이트에대한 초기화를 해줘야 한다.
        //    InitStates(this);
        //    // 처음 실행 시킬 상탱 등록(인스펙터에서 정의 할수도 있고 소스로 정의 할수도 있다.
        //    startState = eState.Idle;
        //}

        //public override void OnEnable()
        //{
        //    //if (controler == null) controler = Hierarchy.i.MonoFSMManager;
        //    if (controler != null) controler.RegistXGameObject(species.ToString(), this);

        //    SetCurrentState(startState);
        //}

        //public override void OnDisable()
        //{
        //    //		bDie = false;
        //    if (controler != null) controler.UnRegistXGameObject(this);
        //    if (spawn != null) spawn.UnRegistXGameObject(this);

        //    this.StopAllCoroutines();
        //}
    }
}