﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using BuzzEngine;

namespace BuzzEngine
{
    public abstract class MonoFSMManager<T> : MonoBehaviour
    {
        //        public enum eTeam
        //        {
        //            Player, Enemy,
        //        }
        //        /// <summary>
        //        /// 게임내 생성된 XGameObject들을 담는다.
        //        /// <para>최상위 첫키는 species Key 이며 2번째 키는 gameObject의 InstanceID이다.</para>
        //        /// <para>기본적으로 XGameObject는 OnEnable시 RegisterCharacter를 이용하여 dicXGameObject에 등록 한다.</para>
        //        /// <para>기본적으로 XGameObject는 Disable시 UnRegisterCharacter를 이용하여 dicXGameObject에 해제 한다.</para>
        //        /// <para>접근시 GetTeam(team name)을 이용하여 접근 할수 있다.</para>
        //        /// </summary>
        //        Dictionary<eTeam, Dictionary<string, FSMCharacter>> dicXGameObject = new Dictionary<eTeam, Dictionary<string, FSMCharacter>>();
        //        /// <summary>
        //        /// 등록 오브젝트 카운팅용.
        //        /// </summary>
        //        Dictionary<eTeam, Dictionary<string, int>> dicRegistCount = new Dictionary<eTeam, Dictionary<string, int>>();
        //        /// <summary>
        //        /// <para>PObject객체를 등록 할수 있다.</para>
        //        /// <para>PObject의 OnEnable함수를 통해 등록 하면 편리 하다.</para>
        //        /// <para>자동으로 dicXGameObject의 첫번째 키를 생성(키가 없는 경우)이후 character.gameObject의 인스턴스아이디를 하위 키로 character를 등록 한다. </para>
        //        /// <para>dicXGameObject[team name].Add(instance id of gameObject,character);</para>
        //        /// </summary>
        //        /// <param name="character"></param>
        //        public void RegistXGameObject(string speciesKey, FSMCharacter character)
        //        {
        //            eTeam teamKey = character.TeamKey;
        //            string instKey = character.gameObject.GetInstanceID().ToString();// 고유하다.

        //            if (dicXGameObject.ContainsKey(teamKey) == false)
        //            {
        //                dicXGameObject.Add(teamKey, new Dictionary<string, FSMCharacter>());
        //                dicRegistCount.Add(teamKey, new Dictionary<string, int>());
        //            }

        //            dicXGameObject[teamKey].Add(instKey, character);

        //            if (dicRegistCount[teamKey].ContainsKey(speciesKey) == false)
        //            {
        //                dicRegistCount[teamKey].Add(speciesKey, 0);
        //            }
        //            dicRegistCount[teamKey][speciesKey]++;
        //#if GAMELOG
        //            Debug.Log(string.Format("Regist team:{0} name:{1} ID:{2} Count:{3}", teamKey, character.name, instKey, dicXGameObject[teamKey].Count));
        //#endif
        //        }
        //        /// <summary>
        //        /// RegisterCharacter를 이용하여 등록한 캐릭터를 제거 한다.
        //        /// <para>PObject의 Disable함수를 통해 등록 하면 편리 하다.</para>
        //        /// </summary>
        //        /// <param name="character"></param>
        //        public void UnRegistXGameObject(FSMCharacter character)
        //        {
        //            eTeam teamKey = character.TeamKey;
        //            string instKey = character.gameObject.GetInstanceID().ToString();

        //            dicXGameObject[teamKey].Remove(instKey);

        //#if GAMELOG
        //            Debug.Log(string.Format("UnRegist team:{0} name:{1} ID:{2} Count:{3}", teamKey, character.name, instKey, dicXGameObject[teamKey].Count));
        //#endif
        //        }
        //        /// <summary>
        //        /// teamName으로 등록된 값을 배열로 리턴 한다.
        //        /// </summary>
        //        /// <param name="teamName"></param>
        //        /// <returns></returns>
        //        public FSMMono<T>[] GetTeam(eTeam _tempKey)
        //        {
        //            if (dicXGameObject.ContainsKey(_tempKey) == false) return null;

        //            return dicXGameObject[_tempKey].Values.ToArray();
        //        }
        //        /// <summary>
        //        /// go에 붙어 있는 PObject 컴포넌트의 인스턴스를 리턴한다.
        //        /// gameObject.GetComponent<RMGPlay.PObject>대신 본 함수를 통해 접근 할 것.
        //        /// </summary>
        //        /// <param name="go"></param>
        //        /// <returns></returns>
        //        public FSMMono GetXGameObjectComponent(GameObject go)
        //        {
        //            string key2 = go.GetInstanceID().ToString();
        //            foreach (var dic1 in dicXGameObject)
        //            {
        //                var dic2 = dic1.Value;
        //                if (dic2.ContainsKey(key2))
        //                {
        //                    return dic2[key2];
        //                }
        //            }
        //            return null;
        //        }
        public abstract IEnumerator Initialize();
        //        /// <summary>
        //        /// 살아 있는 오브젝트의 갯수를 리턴한다.
        //        /// <para>Regist시 증가 , UnRegist시 감소.</para>
        //        /// </summary>
        //        /// <param name="_teamKey"></param>
        //        /// <returns></returns>
        //        public int GetXGameObjectCount(eTeam _teamKey)
        //        {
        //            if (dicXGameObject.ContainsKey(_teamKey) == false) return 0;
        //            return dicXGameObject[_teamKey].Count;
        //        }
        //        /// <summary>
        //        /// 등록한 오브젝트의 갯수를 리턴한다. 같은 species의 총계를 리턴.
        //        /// <para>Regist시 증가 , UnRegist시 변화 없음.</para>
        //        /// </summary>
        //        /// <param name="_teamKey"></param>
        //        /// <param name="_speciesKey"></param>
        //        /// <returns></returns>
        //        public int GetRegistCountOnSpecies(eTeam _teamKey, string _speciesKey)
        //        {
        //            if (dicRegistCount.ContainsKey(_teamKey) == false) return 0;
        //            if (dicRegistCount[_teamKey].ContainsKey(_speciesKey) == false) return 0;

        //            return dicRegistCount[_teamKey][_speciesKey];
        //        }
        //        /// <summary>
        //        /// 등록한 오브젝트의 갯수를 리턴한다, 같은 팀의 총계를 리턴
        //        /// </summary>
        //        /// <param name="_teamKey"></param>
        //        /// <returns></returns>
        //        public int GetRegistCountOnTeam(eTeam _teamKey)
        //        {
        //            if (dicRegistCount.ContainsKey(_teamKey) == false) return 0;

        //            return dicRegistCount[_teamKey].Sum(a => a.Value);
        //        }

        //        public Dictionary<string, FSMCharacter> GetXGameObjects(eTeam _teamKey)
        //        {
        //            return dicXGameObject[_teamKey];
        //        }

    }
}