using UnityEngine;
using System.Collections;
using System;

namespace BuzzEngine
{
    public abstract class FSMState<T>
    {
        public T Target;
        
        public abstract void Enter();

        public abstract void Execute();

        public abstract void Exit();

        public virtual void Next()
        {
            
        }

        public virtual IEnumerator Initialize()
        {
            yield return null;
        }
    }

    public abstract class FSMMonoState<T> : MonoBehaviour where T : FSMMono<T>
    {
        public T Mono;

        public virtual IEnumerator Initialize()
        {
            yield return null;
        }

        public void Exit()
        {
            if (Mono.State != null)
            {
                DestroyObject(this);
            }
        }
        public bool EqualState(Type t)
        {
            return this.GetType() == t;   
        }

        public void ChangeState(Type t)
        {
            Mono.ChangeState(Mono,t);
        }

        public void RevertState()
        {
            Mono.RevertState(Mono);
        }
        //public virtual void Enter(BuzzEngine.XGameObjectMonobehaviour.FSMMono xGameObject) { }

        //public virtual void Exit() { }
    }
}