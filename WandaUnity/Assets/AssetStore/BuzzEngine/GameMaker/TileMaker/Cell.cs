﻿/* Copyright (c) Buzzment. All rights reserved.
 * author       : Heyya
 * date         : 11/08/2013 ~ 11/08/2016
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BuzzEngine.GameMaker.TileMaker
{
    [System.Serializable]
    public class Matrix
    {
        public int X;
        public int Y;

        public Matrix(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    /// <summary>
    /// 타일을 생성한다.
    /// 소속 Cell에 아이템이 비어 있으면 타일을 생성한다.
    /// 1. Create item
    /// 2. add Carrier component
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class Cell : FSMMono<Cell>
    {
        public enum ECellType { Container, Spawner }
        public ECellType CellType = ECellType.Container;

        /// <summary>
        /// Data in Item Prefab
        /// </summary>
        public GameObject ItemPrefab;
        public Image Image { get; set; }
        /// <summary>
        /// ItemCellPrefab Size
        /// </summary>
        public Vector2 Size
        {
            get { return RectTransform.sizeDelta; }
            set { RectTransform.sizeDelta = value; }
        }
        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }
        public bool IsItemDrop = false;
        /// <summary>
        /// ItemCellPrefab position
        /// </summary>
        public Vector2 Position
        {
            get { return RectTransform.anchoredPosition3D; }

            set { RectTransform.anchoredPosition3D = value; }
        }
        public ItemMover CurrentItem;
        public TileMaker TileMaker { get; set; }
        public Matrix MatrixId;

        public Cell NextCell;// { get; set; }

        public Text text;
        public bool IsAutoCreateMover = false;
        public void Awake()
        {
            var s = GetComponentsInChildren<Text>();
            foreach (var t in s)
            {
                text = t;
            }
        }

        public void OnDrawGizmos()
        {
            var size = new Vector3(RectTransform.sizeDelta.x * transform.lossyScale.x, RectTransform.sizeDelta.y * transform.lossyScale.y, 0f);
            Gizmos.DrawWireCube(transform.position, size);
        }
        public void Init(ECellType cellType, Vector2 position, RectTransform parent, Vector2 tileSize, bool useCellImage, GameObject itemPrefab, TileMaker tileMaker, Matrix id)
        {
            CellType = cellType;
            ItemPrefab = itemPrefab;
            TileMaker = tileMaker;
            MatrixId = id;
            // add component
            if (useCellImage)
            {
                gameObject.AddComponent<Image>();
                var outline = gameObject.AddComponent<Outline>();
                outline.effectColor = Color.black;
                outline.effectDistance = Vector2.one * 3;
            }

            RectTransform.SetParent(parent, false);
            RectTransform.anchoredPosition3D = position;
            RectTransform.SetParent(parent);
            Size = tileSize;

            switch (cellType)
            {
                case ECellType.Spawner:
                    ChangeState(this, typeof(CellStateSpawn));
                    break;
                case ECellType.Container:
                    ChangeState(this, typeof(CellStateOpen));
                    break;
                default:
                    ChangeState(this, typeof(CellStateOpen));
                    break;
            }
        }
        /// <summary>
        /// 아이템을 받을수 있는 상태를 판단하여 리턴한다.
        /// </summary>
        /// <returns></returns>
        public bool CanReceive()
        {
            return EqualState(typeof(CellStateOpen)) || EqualState(typeof(CellStateCarry));
        }

        public void SetItem(ItemMover item)
        {
            CurrentItem = item;
            ChangeState(this, typeof(CellStateFull));
        }
    }

    public class CellState : FSMMonoState<Cell>
    {
        public void Start()
        {
            var s = Mono.CurrentItem == null ? "Item : null" : Mono.CurrentItem.name;
            Mono.text.text = Mono.StateName + "\n" + s;
            Mono.text.text = "";
        }
        public ItemMover CurrentItem { get { return Mono.CurrentItem; } }
        public void SendItem(Cell target, ItemMover item)
        {
            target.CurrentItem = item;
            Mono.CurrentItem = null;
        }
    }
    public class CellStateSpawn : CellState
    {
        public void Update()
        {
            // Not use auto create system
            if (Mono.TileMaker.IsMaking == false) return;
            // Is exist the next cell?
            if (Mono.NextCell == null) return;

            // ####################################################
            if (Mono.CurrentItem != null) return;
            // Create Item
            Mono.CurrentItem = CreateItem();
            // change state
            ChangeState(typeof(CellStateFull));
            //######################################################
        }
        
        private ItemMover CreateItem()
        {
            var item = Instantiate(Mono.ItemPrefab);
            var mover = item.GetComponent<ItemMover>();

            if (mover == null)
            {
                mover = item.AddComponent<ItemMover>();
                Mono.IsAutoCreateMover = true;
            }
            else
            {
                Mono.IsAutoCreateMover = false;
            }

            item.transform.SetParent(Mono.transform.parent, false);
            item.transform.localPosition = Mono.transform.localPosition + Vector3.up;
            item.name = "Item" + Time.time;

            return mover;
        }
    }
    /// <summary>
    /// 아이템이 비워져 있고 아이템을 받을수 있는 상태 : Close아이템을 않 받음. Full아이템이 들어 있음
    /// </summary>
    public class CellStateOpen : CellState
    {
        public new void Start()
        {
            base.Start();

            if (Mono.CellType != Cell.ECellType.Spawner) return;

            Mono.ChangeState(Mono,typeof(CellStateSpawn));
        }
    }
    /// <summary>
    /// 아이템이 들어 있는 상태.
    /// </summary>
    public class CellStateFull : CellState
    {
        public void Update()
        {
            if (Mono.CurrentItem == null)
            {
                Mono.ChangeState(Mono,typeof(CellStateOpen));
                return;
            }

            // If next cell is empty, send item to next cell
            if (!Mono.NextCell) return;
            if (!Mono.NextCell.CanReceive()) return;
            
            ChangeState(typeof(CellStateCarryWait));
        }
    }
    public class CellStateCarryWait : CellState
    {
        private float _timer = 0.01f;

        public void Update()
        {
            _timer -= Time.deltaTime;

            if (!(_timer <= 0)) return;
            Mono.CurrentItem.DoMove(Mono, Mono.NextCell);
            ChangeState(typeof(CellStateCarry));
        }
    }
    public class CellStateCarry : CellState
    {
    }
}