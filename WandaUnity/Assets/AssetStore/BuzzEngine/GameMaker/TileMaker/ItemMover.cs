﻿using UnityEngine;

namespace BuzzEngine.GameMaker.TileMaker
{
    public class ItemMover : FSMMono<ItemMover>
    {
        public float Speed;
        public Cell CurrentCell;
        public Cell GoalCell;
        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }
        public void DoMove(Cell currentCell,Cell goalCell)
        {
            CurrentCell = currentCell;
            GoalCell = goalCell;
            Speed = CurrentCell.IsAutoCreateMover ? currentCell.TileMaker.ItemDropSpeed : Speed;
            ChangeState(this, typeof(ItemMoveStateMove));
        }
    }

    public class ItemMoverState : FSMMonoState<ItemMover>
    {
    }

    public class ItemMoveStateIdle : ItemMoverState
    {
        public void Start()
        {
            Mono.GoalCell = null;
        }
    }

    public class ItemMoveStateMove : ItemMoverState
    {
        public void Update()
        {
            if (!Mono.GoalCell)
            {
                Mono.ChangeState(Mono,typeof(ItemMoveStateIdle));
                return;
            }

            var goalPos = Mono.GoalCell.transform.localPosition;
            var itemPos = transform.localPosition;
            var addPos = (goalPos - itemPos).normalized * Mono.Speed * Time.deltaTime;

            // arrived : 움직일 거리 >= 도착 거리
            if (Vector2.Distance(itemPos + addPos, itemPos) >= Vector2.Distance(goalPos, itemPos))
            {

                // 다음 다음 셀의 상태가 열려 있다.
                if (Mono.GoalCell.NextCell && Mono.GoalCell.NextCell.CanReceive())
                {
                    transform.localPosition += addPos;
                    ChangeGoallCell();
                }
                else
                {
                    transform.localPosition = goalPos;
                    ArrivedGoallCell();
                }
            }
            // moving
            else
            {
                transform.localPosition += addPos;
            }
        }

        private void ArrivedGoallCell()
        {
            var preCell = Mono.CurrentCell;
            var curCell = Mono.GoalCell;

            // 직전 셀 초기화
            preCell.CurrentItem = null;

            preCell.ChangeState(preCell, typeof(CellStateOpen));

            // 현재 셀 교체 목적지 셀로 교체
            curCell.CurrentItem = Mono;
            // 변경된 셀의 속성 변경
            curCell.ChangeState(curCell, typeof(CellStateFull));

            Mono.ChangeState(Mono, typeof(ItemMoveStateIdle));

            // 소집 해제
            Mono.CurrentCell = Mono.GoalCell;
            Mono.GoalCell = null;
        }

        private void ChangeGoallCell()
        {
            var preCell = Mono.CurrentCell;
            var curCell = Mono.GoalCell;

            // 직전 셀 초기화
            preCell.CurrentItem = null;

            preCell.ChangeState(preCell, typeof(CellStateOpen));

            // 현재 셀 교체 목적지 셀로 교체
            curCell.CurrentItem = Mono;
            // 변경된 셀의 속성 변경
            curCell.ChangeState(curCell, typeof(CellStateFull));
            
            // 소집 해제
            Mono.CurrentCell = Mono.GoalCell;
            Mono.GoalCell = Mono.GoalCell.NextCell;
        }
    }
}
