﻿using System.Collections.Generic;
using BuzzEngine.XGameObject.Template;
using UnityEngine;
using UnityEngine.UI;

namespace BuzzEngine.GameMaker.TileMaker
{
    public class TileMaker : MonoBehaviour
    {
        public enum ECellType { Item, Dropper}
        public ECellType CellType = ECellType.Item;

        /// <summary>
        /// 가로세로칸을 정의 한다.
        /// </summary>
        public Matrix Matrix = new Matrix(4,5);
        /// <summary>
        /// Created cell array by CreateItemCells
        /// </summary>
        public Cell[,] ItemCells;
        public Cell[] SpawnCells;

        /// <summary>
        /// RectTransform instance
        /// </summary>
        private RectTransform _rectTransform;
        /// <summary>
        /// _rectTransform
        /// </summary>
        public RectTransform RectTransform
        {
            get { return _rectTransform ?? (_rectTransform = GetComponent<RectTransform>()); }
        }
        
        /// <summary>
        /// 타일의 크기를 정의한다.
        /// </summary>
        public Vector2 TileSize = new Vector2(100,100);
        /// <summary>
        /// 현재 패널의 사이즈를 기준으로 타일 사이즈를 자동으로 만들어 준다.
        /// </summary>
        public bool UseAutoTileSize = false;

        public bool UseCellImage = false;
        /// <summary>
        /// Cell에 담을 아이템
        /// </summary>
        public GameObject ItemPrefab = null;
        public GameObject ItemCellPrefab = null;
        public GameObject DropCellPrefab = null;

        public bool IsMaking = false;
        public float ItemDropSpeed = 50f;

        /// <summary>
        /// Unity Awake
        /// </summary>
        public void Awake()
        {
            CreateItemCells();
        }
        /*
        /// <summary>
        /// 셀의 중심점을 잡아 준다.
        /// </summary>
        //private RectTransform CreateCellAnchor()
        //{
        //    const string anchorName = "Anchor";
        //    var pre = GetComponentsInChildren<RectTransform>();
        //    foreach (var rt in pre)
        //    {
        //        if (!rt.name.Equals(anchorName)) continue;
        //        DestroyObject(rt.gameObject);
        //        break;
        //    }

        //    var go = new GameObject(anchorName);
        //    var rect = go.AddComponent<RectTransform>();

        //    rect.anchorMin = rect.anchorMax = Vector2.zero;
        //    rect.sizeDelta = rect.pivot = Vector2.zero;

        //    rect.SetParent(transform, false);

        //    return rect;
        //}
        */
        /// <summary>
        /// 셀의 중심점을 잡아 준다.
        /// </summary>
        private RectTransform CreateCellParent()
        {
            var basePos = Vector2.zero;
            basePos.x = -((Matrix.X * TileSize.x))/2f;
            basePos.y = -((Matrix.Y * TileSize.y))/2f;

            const string anchorName = "CellParent";
            var pre = GetComponentsInChildren<RectTransform>();
            foreach (var rt in pre)
            {
                if (!rt.name.Equals(anchorName)) continue;
                DestroyObject(rt.gameObject);
                break;
            }
            
            var rect = new GameObject(anchorName).AddComponent<RectTransform>();

            rect.anchorMin = rect.anchorMax = new Vector2(0.5f, 0.5f);
            rect.sizeDelta = Vector2.zero;
            rect.anchoredPosition = basePos;

            rect.SetParent(transform, false);

            return rect;
        }
        
        /// <summary>
        /// Matrix의 갯수를 기준으로 cell들을 생성한다.
        /// </summary>
        public void CreateItemCells()
        {
            CreateItemCells(RectTransform.sizeDelta);
        }
        /// <summary>
        /// Matrix의 갯수를 기준으로 cell들을 생성한다.
        /// </summary>
        public void CreateItemCells(Vector2 boardSize)
        {
            ClearCells();
            if (UseAutoTileSize)
            {
                CalculateTileSize();
            }
            //var parent = CreateCellAnchor();
            var parent = CreateCellParent();

            ItemCells = new Cell[Matrix.X,Matrix.Y];
            SpawnCells = new Cell[Matrix.X];

            for (var x = 0; x < Matrix.X; x++)
            {
                // Movement
                SpawnCells[x] = CreateCell(Cell.ECellType.Spawner, new Matrix(x,Matrix.Y), parent, ItemPrefab);
                SpawnCells[x].name = "DropCell" + x.ToString();

                for (var y = 0; y < Matrix.Y; y++)
                {
                    ItemCells[x, y] = CreateCell(Cell.ECellType.Container, new Matrix(x, y), parent, null);
                    ItemCells[x, y].name = "ItemCell" + x.ToString() + y.ToString();
                }
            }

            InitNextCells();
        }

        public void InitNextCells()
        {
            for (var x = 0; x < Matrix.X; x++)
            {
                // Movement
                SpawnCells[x].NextCell = ItemCells[x, Matrix.Y - 1];

                for (var y = 1; y < Matrix.Y; y++)
                {
                    ItemCells[x, y].NextCell = ItemCells[x, y - 1];
                }
            }
        }
        private void CalculateTileSize()
        {
            Vector2 boardSize = RectTransform.sizeDelta;
            TileSize.x = boardSize.x/Matrix.X;
            TileSize.y = boardSize.y/Matrix.Y;
        }

        /// <summary>
        /// Create a cell and set position
        /// 중앙 정렬한다.
        /// </summary>
        /// <remarks>??</remarks>
        private Cell CreateCell(Cell.ECellType cellType,Matrix id, RectTransform parent, GameObject itemPrefab )
        {
            var pos = TileSize / 2f + new Vector2(id.X * TileSize.x, id.Y * TileSize.y);
            Cell cell = null;

            if (ItemCellPrefab == null)
            {
                cell = new GameObject().AddComponent<Cell>();
                //cell.Init(pos, parent, TileSize);
                cell.Init(cellType, pos , parent, TileSize, UseCellImage,ItemPrefab,this,id);
            }
            else
            {
                var go = Instantiate(cellType == Cell.ECellType.Container ? ItemCellPrefab : DropCellPrefab);
                cell = GetComponent<Cell>() ?? go.AddComponent<Cell>();
                cell.Init(cellType, pos, parent, TileSize, false, itemPrefab,this,id);
            }
            
            // Movement is add commponent dropper
            if (CellType == ECellType.Dropper)
            {
                //var dropper = cell.gameObject.AddComponent<Movement>();
                //dropper.Init(ItemPrefab,);
            }
            

            return cell;
        }
   
        /// <summary>
        /// CreateCells을 통해 생성된 Cell을 제거 한다.
        /// </summary>
        public void ClearCells()
        {
            if (ItemCells == null) return;

            foreach (var c in ItemCells)
            {
                if( c != null)
                    DestroyImmediate(c.gameObject);
            }
            ItemCells = null;
        }
    }


}
