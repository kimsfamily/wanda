﻿using UnityEditor;
using UnityEngine;
using BuzzEditor;

namespace BuzzEngine.GameMaker.TileMaker.Editor
{
    [CustomEditor(typeof(TileMaker))]

    public class TileMakerEditor : BzEditor
    {
        private TileMaker _i = null;

        protected override void OnEnable()
        {
            base.OnEnable();
            _i = (TileMaker) target;

            EditorUtility.SetDirty(_i);
        }

        public override void OnInspectorGUI()
        {
            //float bWidth = 200f;

            //base.OnInspectorGUI();
            //{
            //    EditorGUILayout.Separator();
            //}

            _i.CellType = (TileMaker.ECellType)EditorGUILayout.EnumPopup("CellType", _i.CellType);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("Matrix"));

            _i.UseAutoTileSize = EditorGUILayout.Toggle("UseAutoTileSize", _i.UseAutoTileSize);

            if(_i.UseAutoTileSize == false)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("TileSize"));

            if (_i.CellType == TileMaker.ECellType.Item)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("ItemPrefab"));

            _i.ItemCellPrefab = (GameObject)EditorGUILayout.ObjectField("Item Cell Prefab", _i.ItemCellPrefab, typeof(GameObject), true);
            if (_i.ItemCellPrefab == null)
            {
                _i.UseCellImage = EditorGUILayout.Toggle("UseCellImage", _i.UseCellImage);
            }
            
            _i.DropCellPrefab = (GameObject)EditorGUILayout.ObjectField("Drop Cell Prefab", _i.DropCellPrefab, typeof(GameObject), true);


            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsMaking"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ItemDropSpeed"));

            //EditorGUILayout.BeginHorizontal();
            //{
            //    if (GUILayout.Button("Load Dababase", GUILayout.Width(bWidth)))
            //    {
            //        _i.LoadDataBase();
            //        EditorUtility.SetDirty(_i);
            //    }
            //    if (GUILayout.Button("Save Dababase", GUILayout.Width(bWidth)))
            //    {
            //        _i.SaveDataBase();
            //    }
            //}
            //EditorGUILayout.EndHorizontal();

            //update and redraw:
            if (GUI.changed)
            {
                EditorUtility.SetDirty(_i);
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
