﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wanda.Utils
{
    public class ChracterMove : MonoBehaviour
    {
        public float MoveSpeed;
        public Vector3 Movement;
        
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                transform.localPosition = Vector3.zero;
            }
            Move();
        }

        void Move()
        {
            Movement.x = Input.GetAxis("Horizontal");
            Movement.y = Input.GetAxis("Vertical");

            transform.Translate(Movement * MoveSpeed * Time.deltaTime);
        }
    }
}
