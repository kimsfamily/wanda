﻿using BuzzEngine;
using Squeezin.InGame.XGameObjects;
using BuzzEngine.GameMaker.TileMaker;
using UnityEngine;

namespace Squeezin.InGame
{
    public class InGame : FSMMonoSingleton<InGame>
    {
        public TileMaker TileMaker = null;
        public TileManager TileManager = null;
        public UserInterface Ui = null;
        public Board Board = new Board();

        // Use this for initialization
        private void Start()
        {
            var debug = gameObject.AddComponent<Debug>();
            debug.InGame = this;
            SetState(this,new Start());
            
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            
            InitBoard();
        }

        public void InitBoard()
        {
            Board.ScoreNow = 0;
            Board.ScoreBest = PlayerPrefs.GetInt("ScoreBest", 0);

            Ui.ScoreBest.text = Board.ScoreBest.ToString();
            Ui.ScoreNow.text = "0";
        }

        // Update is called once per frame
        private void Update()
        {

        }

        public void AddScore(int add)
        {
            Board.ScoreNow += add;
            Ui.ScoreNow.text = Board.ScoreNow.ToString();

            if (Board.ScoreNow > Board.ScoreBest)
            {
                Board.ScoreBest = Board.ScoreNow;
                Ui.ScoreBest.text = Board.ScoreBest.ToString();


                PlayerPrefs.SetInt("ScoreBest",Board.ScoreBest);
            }
        }
    }
}

