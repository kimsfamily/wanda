﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using BuzzEngine;

namespace Squeezin.InGame
{
    public class Link : BzMonoSingleton<Link>
    {
        //public AppInGameState state { get; set; }

        //public AppInGameState.eStateKey startStateKey;

        //public AppCtrlUI uiCtrl { get; set; }
        //public AppCtrlCamera cameraCtrl { get; set; }
        //public AppCtrlMyCharacter characterCtrl { get; set; }
        //public AppCtrlTiledMap tiledMapCtrl { get; set; }

        //public MyData data = new MyData();

        public bool isPause = false;

        public override void Init()// from singleton
        {
            base.Init();

            //UnityEngine.SceneManagement.SceneManager.LoadScene("InGameUI");

            StartCoroutine(LoadFiles());
        }

        IEnumerator LoadFiles()
        {
            // this code is loading
            isLoaded = false;

            //yield return Application.LoadLevelAdditiveAsync("AppInGame_UIZone"); //SceneManager.LoadSceneAsync("AppInGame_UIZone");

            //state = gameObject.GetComponent<AppInGameState>();

            // 2. connect component
            //tiledMapCtrl = FindObjectOfType<AppCtrlTiledMap>();
            //cameraCtrl = FindObjectOfType<AppCtrlCamera>();

            //characterCtrl = gameObject.AddComponent<AppCtrlMyCharacter>();


            // 2. load resource
            yield return BzSoundManager.i.WaitLoadedFile();

            isLoaded = true;

            // All load 
            //state.ChangeState(startStateKey);

            yield break;
        }

        public bool isLoaded = false;

        public Coroutine WaitLoaded()
        {
            return StartCoroutine(iWaitLoaded());
        }
        IEnumerator iWaitLoaded()
        {
            while (isLoaded == false)
            {
                yield return new WaitForEndOfFrame();
            }

            yield return true;
        }

    }
}