﻿using System;
using Squeezin.InGame.XGameObjects;
using UnityEngine;

namespace Squeezin.InGame
{
    public class Debug : MonoBehaviour
    {
        public InGame InGame = null;

        public void OnGUI()
        {
            var buttonSize = new Vector2(Screen.width / 10f, Screen.height / 16f);
            var xid = 0;
            var yid = 0;
            
            if (Button(xid++, yid, buttonSize, InGame.StateName))
            {
                InGame.NextState();
            }
            /*
            if (Button(xid++, yid, buttonSize, InGame.I.TileManager.CheckRule.ToString()))
            {
                var c = (int) InGame.I.TileManager.CheckRule;
                c = ++c >= (int)TileManager.ECheckRule.Count ? 0 : c;
                InGame.I.TileManager.CheckRule = (TileManager.ECheckRule) c;
            }
            if (Button(xid++, yid, buttonSize, "Reset"))
            {
                InGame.I.InitBoard();
                InGame.I.TileMaker.CreateItemCells();
            }
            if (Button(xid++, yid, buttonSize, "Size." + (InGame.I.TileMaker.UseAutoTileSize ? "On" : "Off")))
            {
                InGame.I.TileMaker.UseAutoTileSize = !InGame.I.TileMaker.UseAutoTileSize;
            }
            if (Button(xid, yid, buttonSize.x, buttonSize.y / 2f, "M.x " + InGame.TileMaker.Matrix.X))
            {
                InGame.TileMaker.Matrix.X = InGame.TileMaker.Matrix.X + 1 > 10 ? 2 : InGame.TileMaker.Matrix.X + 1;
            }
            if (Button(xid++, yid + 1, buttonSize.x, buttonSize.y / 2f, "M.x " + InGame.TileMaker.Matrix.X))
            {
                InGame.TileMaker.Matrix.X = InGame.TileMaker.Matrix.X - 1 < 0 ? 10 : InGame.TileMaker.Matrix.X - 1;
            }

            if (Button(xid, yid, buttonSize.x, buttonSize.y / 2f, "M.y " + InGame.TileMaker.Matrix.Y))
            {
                InGame.TileMaker.Matrix.Y = InGame.TileMaker.Matrix.Y + 1 > 10 ? 2 : InGame.TileMaker.Matrix.Y + 1;
            }
            if (Button(xid++, yid + 1, buttonSize.x, buttonSize.y / 2f, "M.y " + InGame.TileMaker.Matrix.Y))
            {
                InGame.TileMaker.Matrix.Y = InGame.TileMaker.Matrix.Y - 1 < 0 ? 10 : InGame.TileMaker.Matrix.Y - 1;
            }

            // Tile Id Min
            int min = InGame.TileManager.TileIdMin;
            int max = InGame.TileManager.TileIdMax;
            if (Button(xid, yid, buttonSize.x, buttonSize.y / 2f, "No.min " + min))
            {
                if (min + 1 < max)
                    min = min + 1;
            }
            if (Button(xid++, yid + 1, buttonSize.x, buttonSize.y / 2f, "No.min " + min))
            {
                if (min > 0)
                    min -= 1;
            }
            // Time Id Max
            if (Button(xid, yid, buttonSize.x, buttonSize.y / 2f, "No.min " + max))
            {
                if (max < 20)
                    max = max + 1;
            }
            if (Button(xid++, yid + 1, buttonSize.x, buttonSize.y / 2f, "No.max " + max))
            {
                if (max - 1 > min)
                    max -= 1;
            }
            InGame.TileManager.TileIdMin = min;
            InGame.TileManager.TileIdMax = max;

            if (Button(xid++, yid, buttonSize, "No.Re"))
            {
                InGame.TileManager.TileIdMin = 1;
                InGame.TileManager.TileIdMax = 4;
                InGame.TileManager.Matrix.x = 4;
                InGame.TileManager.Matrix.y = 5;
                InGame.TileManager.CreateTiles();
            }*/
        }

        private bool Button(float x, float y, Vector2 size, string name)
        {
            return GUI.Button(new Rect(x * size.x, y * size.y, size.x, size.y), name);
        }
        private bool Button(float x, float y, float w, float h, string name)
        {
            return GUI.Button(new Rect(x * w, y * h, w, h), name);
        }
    }
}
