﻿using System;
using BuzzEngine;

namespace Squeezin.InGame
{
    public class AppState : FSMState<InGame>
    {
        public override void Enter()
        {
            //throw new NotImplementedException();
        }

        public override void Execute()
        {
            //throw new NotImplementedException();
        }

        public override void Exit()
        {
            //throw new NotImplementedException();
        }
    }

    public class Start : AppState
    {
        public override void Enter()
        {
            Target.TileMaker.CreateItemCells();
        }

        public override void Next()
        {
            Target.SetState(Target,new Ready());
        }
    }
    public class Ready : AppState
    {
        public override void Next()
        {
            Target.SetState(Target, new Play());
        }
    }
    public class Play : AppState
    {
        public override void Next()
        {
            Target.SetState(Target, new GameOver());
        }
    }
    public class GameOver : AppState
    {
        public override void Next()
        {
            Target.SetState(Target, new Start());
        }
    }
}
