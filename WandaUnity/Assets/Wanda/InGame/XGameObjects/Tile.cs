﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using BuzzEngine;
using BuzzEngine.GameMaker.TileMaker;

namespace Squeezin.InGame.XGameObjects
{
    public class Tile : FSMMono<Tile>
    {
        #region State & Species
        
        public enum eSpecies
        {
            Normal,
            Count,
        }

        public int GetSpeciesCount()
        {
            return (int)eSpecies.Count;
        }

        public string GetSpeciesFromIndex(int index)
        {
            return ((eSpecies)index).ToString();
        }

        #endregion

        public RectTransform RectTransform = null;
        public BoxCollider2D BoxCollider2D = null;
        public Text Text = null;
        public Image Image = null;
        public ItemMover ItemMover = null;

        //public BoxCollider2D BoxCollider2D = null;
        public Color OriginColor { get; set; }
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                Text.text = _id.ToString();
            }
        }
        
        public eSpecies species = eSpecies.Count;

        public void Start()
        {
            OriginColor = Image.color;
            ChangeState(this, typeof(TileStateIdle));

            Id = InGame.I.TileManager.GetTileId();

            BoxCollider2D.size = 
                RectTransform.sizeDelta = InGame.I.TileMaker.TileSize * 0.9f;
        }

        public void Init(Vector3 position, RectTransform parent,Vector2 tileSize)
        {
            RectTransform.SetParent(parent,false);
            RectTransform.anchoredPosition3D = position;
            BoxCollider2D.size = 
                RectTransform.sizeDelta = tileSize;
        }

        public void Update()
        {
            if (ItemMover == null) ItemMover = GetComponent<ItemMover>();

            if (ItemMover == null) return;

            //Text.text = ItemMover.StateName;
        }
        //public override void Awake()
        //{
        //    AddStateDic(eState.Idle, new TileStateIdle());
        //    // 모든 스테이트를 등록 후 반드시 각 스테이트에대한 초기화를 해줘야 한다.
        //    InitStates(this);
        //    // 처음 실행 시킬 상탱 등록(인스펙터에서 정의 할수도 있고 소스로 정의 할수도 있다.
        //    startState = eState.Idle;
        //}

        //public override void OnEnable()
        //{
        //    //if (controler == null) controler = Hierarchy.i.Manager;
        //    if (controler != null) controler.RegistXGameObject(species.ToString(), this);

        //    SetCurrentState(startState);
        //}

        //public override void OnDisable()
        //{
        //    //		bDie = false;
        //    if (controler != null) controler.UnRegistXGameObject(this);
        //    if (spawn != null) spawn.UnRegistXGameObject(this);

        //    this.StopAllCoroutines();
        //}
    }
}