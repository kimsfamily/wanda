﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Linq;
using BuzzEngine;
using BuzzEngine.GameMaker.TileMaker;

namespace Squeezin.InGame.XGameObjects
{
    public abstract class TileState : FSMMonoState<Tile>
    {
    }

    public class TileStateIdle : TileState
    {
        public void Start()
        {
            Mono.Image.color = Mono.OriginColor;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (!Input.GetMouseButton(0)) return;

            var tiles = InGame.I.TileManager.ClickTiles;
            TileManager man = InGame.I.TileManager;

            if (tiles.Count <= 0 || Mono.Id == tiles.Last().Id)
            {
                Mono.ChangeState(Mono, typeof(TileStatePress));
                
                if (InGame.I.TileManager.LastTile && InGame.I.TileManager.LastTile != Mono)
                    InGame.I.TileManager.LastTile.ChangeState(InGame.I.TileManager.LastTile, typeof(TileStateIdle));
            }
        }

        public bool CheckRule()
        {
            var tiles = InGame.I.TileManager.ClickTiles;
            var last = InGame.I.TileManager.LastTile;

            if (tiles.Count <= 0 || Mono.Id == tiles.Last().Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsFirstTile()
        {
            return InGame.I.TileManager.ClickTiles.Count <= 0;
        }
    }

    public class TileStatePress : TileState
    {
        public void Start()
        {
            Mono.Image.color = Color.cyan;
            InGame.I.TileManager.ClickTiles.Add(Mono);
        }
    }
    public class TileStatePoint : TileStateIdle
    {
        public new void Start()
        {
            Mono.Image.color = Color.red;
        }
    }
}