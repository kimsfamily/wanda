﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BuzzEngine;

namespace Squeezin.InGame.XGameObjects
{
    public class TileManager : MonoFSMManager<TileManager>
    {
        public enum ECheckRule
        {
            Liner,Around,Cross,Count
        }
        public Tile PObjPrefab;
        public ECheckRule CheckRule = ECheckRule.Cross;
        public List<Tile> ClickTiles = new List<Tile>();
        public Tile LastTile = null;
        public Vector2 Matrix = new Vector2(4,5);
        public Vector2 BoardSize = new Vector2(600,800);
        public RectTransform RectTransform = null;

        public override IEnumerator Initialize()
        {
            //if (pObjPrefab == null)
            //{
            //    yield return RMGResource.i.Load<Tile>(RMG.GameTitles.AAplication, RMGResource.ePath.PlayObjects, "kingkong", v => pObjPrefab = v);
            //    if (pObjPrefab == null) { Debug.LogError("Not found resource : " + "kingkong"); yield break; }
            //}
            yield break;
        }

        private RectTransform CreateTileAnchor()
        {
            var anchorName = "Anchor";
            var pre = GetComponentsInChildren<RectTransform>();
            foreach (var rt in pre)
            {
                if (rt.name.Equals(anchorName))
                {
                    DestroyObject(rt.gameObject);
                    break;
                }
            }

            var go = new GameObject(anchorName);
            var rect = go.AddComponent<RectTransform>();

            rect.anchorMin = rect.anchorMax = Vector2.zero;
            rect.sizeDelta = rect.pivot = Vector2.zero;

            rect.SetParent(transform,false);

            return rect;
        }

        public void CreateTiles()
        {
            CreateTiles(RectTransform.sizeDelta);
        }
        public void CreateTiles(Vector2 boardSize)
        {
            var parent = CreateTileAnchor();
            var tileSize = GetTileSize();

            for (var x = 0; x < Matrix.x; x++)
            {
                for (var y = 0; y < Matrix.y; y++)
                {
                    CreateTile(x,y,parent,tileSize);
                }
            }    
        }

        private void CreateTile(int xid, int yid,RectTransform parent,Vector2 tileSize)
        {
            var tile = Instantiate(PObjPrefab);
            var position = (Vector3)tileSize/2f + new Vector3(xid * tileSize.x, yid * tileSize.y, 0f);
            tile.Init(position,parent,tileSize);
        }

        private Vector2 GetTileSize()
        {
            return new Vector2()
            {
                x = RectTransform.sizeDelta.x/Matrix.x,
                y = RectTransform.sizeDelta.y/Matrix.y
            };
        }

        public int TileIdMin = 1;
        public int TileIdMax = 4;

        public int GetTileId()
        {
            return Random.Range(TileIdMin, TileIdMax + 1);
        }

        public void TileUp()
        {
            StartCoroutine(ITileUp());
        }
        public IEnumerator ITileUp()
        {
            var len = ClickTiles.Count;

            if (len > 1)
            {
                for (var i = 0; i < len - 1; i++)
                {
                    yield return new WaitForSeconds(0.1f);
                    DestroyImmediate(ClickTiles[i].gameObject);

                    InGame.I.AddScore(1+i);
                }
                yield return new WaitForSeconds(0.3f);
                ClickTiles[len - 1].ChangeState(ClickTiles[len - 1], typeof(TileStatePoint));
                ClickTiles[len - 1].Id++;

                LastTile = ClickTiles[len - 1];
            }
            else
            {
                if(ClickTiles.Count > 0)
                    ClickTiles[0].ChangeState(ClickTiles[0], typeof(TileStateIdle));
            }
            
            ClickTiles.Clear();   
        }

        public void Update()
        {
            if (!Input.GetMouseButtonUp(0)) return;
            
            InGame.I.TileManager.TileUp();
        }
    }

}