﻿using UnityEngine;
using System.Collections;

public class MyTouch : MonoBehaviour
{
    public Camera cm;
    public BoxCollider2D box;

	// Use this for initialization
    public void Start ()
    {
        box.enabled = false;
    }
	
	// Update is called once per frame
    public void Update ()
	{
	    if (Input.GetMouseButton(0))
	    {
            box.enabled = true;

            var pos = cm.ScreenToWorldPoint(Input.mousePosition);

            transform.position = pos;
        }
        else if (Input.GetMouseButtonUp(0))
	    {
	        box.enabled = false;
	    }
	}


}
