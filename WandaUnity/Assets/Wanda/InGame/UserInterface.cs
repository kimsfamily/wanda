﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Squeezin.InGame
{
    public class UserInterface : MonoBehaviour
    {
        public Text ScoreNow;
        public Text ScoreBest;

        public void Start()
        {
            ScoreNow.text = "0";
        }
    }
}
